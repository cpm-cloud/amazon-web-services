# AWS Certified Cloud Practitioner

Reference: A Cloud Guru course [AWS Certified Cloud Practitioner 2020](https://acloud.guru/learn/aws-certified-cloud-practitioner)

## Cloud Computing
- 6 advantages of cloud:
    1. Pay only for what you use instead of having to invest heavily in data centers.
    1. Benefit from massive economies of scale.
    1. Stop guessing about capacity.
    1. Increase speed and agility.
    1. Stop spending money running and maintaining data centers.
    1. Go global in minutes.
- 3 types of cloud computing:
    - IaaS: you manage the server which can be physical or virtual, as well as th OS. e.g. EC2.
    - PaaS: someone else manages the underlying hardware and OS. You just focus on your applications. e.g., Heroku.
    - SaaS: all you worry about is the software itself and how you want to use it. e.g. Gmail.
- Region and AZ
    - Availability Zone (AZ) may be several data centers, but because they are close together, they are counted as 1 AZ.
    - A Region is a geographical area. Each Region consists of 2 (or more) AZs.
    - Edge Locations are endpoints for AWS used for caching content. This consists of CloudFront and CDN.
    - Amount: Region < AZ < Edge Locations.
- Creating a billing alarm (only available in **us-east-1**):
    1. You may need to create any service first, like EC2.
    1. CloudWatch -> Alarm -> Billing.
    1. Create alarm -> set up more than 10 USD then alert -> go next.
    1. Create new topic (for SNS) and give it a name -> input your email.
    1. Review and create alarm.
    1. If your state is insufficient data, that's because the default calculation needs 6 hour data, so just wait it.
    1. Go to your email and confirm.

## IAM
- Go to IAM and set up your account to let 5 security status all green.
- IAM stands for **Identity Access Management**. It's **Global** service.
- A **Group** is simply a place to store your users. Your users will inherit all permissions that the group has.

## S3
- S3 is a safe place to store your files.
- S3 is **Object-based** storage:
    - Key
    - Value
    - Version ID (S3 has versioning)
    - Metadata
    - Subresources (ACL and torrent)
- The data is spread across multiple devices and facilities.
- A File in S3 can be from 0 Bytes to 5 TB.
- There's is unlimited storage.
- Files are stored in **Buckets**.
- S3 is an **universal namespace**.
- If you update an ***EXISTING*** file or delete a file and read it immediately, you may get the older version, or you may not.
- S3 secures your data using ACL(file level) and Bucket policies(bucket level).
- S3 store classes (more details see [here](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html))
    1. S3 Standard
    1. S3 - IA(infrequently accessed)
    1. S3 One Zone - IA
    1. S3 - Intelligent Tiering: designed to optimize costs by automatically moving data to the most cost-effective access tier.
    1. S3 - Glacier
    1. S3 - Glacier Deep Archive
- Create an S3 bucket
    1. Services -> S3 -> Create bucket
    1. Fill out the bucket name and choose the region you want to place and go next
    1. Set up your configuration (you can go next directly)
    1. Set up permission (unchecked the all blocking will make your bucket public)
    1. Finish and create a bucket
    1. Click a bucket you created and upload the file to test it.
    1. If you cannot open file after uploading, you can go to Actions -> Make public
- The bucket and file have their own properties and permissions.
- You can replicate the contents of one bucket to another bucket automatically by using cross region replication (Choose your bucket -> Management -> Replication).
- Restricting Bucket Access
    - Bucket Policies: applies across the whole bucket.
    - Object Policies: applies to individual files.
    - IAM Policies to Users & Groups: applies to users & group.
- Create an S3 website:
    1. Services -> S3
    1. Create a bucket by default
    1. Upload 2 files named `index.html` and `error.html` to show your website and error page.
    1. Make 2 files public.
    1. Go to your bucket -> Properties -> Static website hosting -> Use this bucket to host a website
    1. Place your `index.html` in index document and `error.html` in error document.
    1. Save
- S3 Scales automatically to meet your demand. Many enterprises will put static websites on S3 when they think there's going to be a large number of requests (such as for a movie preview for example).

## CloudFront
- It's AWS CDN (content delivery network) service.
- Key Terminology:
    - **Edge location**: this is the location where content will be cached.
    - **Origin**: this is the origin of all the files that the CDN will distribute. This can be an S3 Bucket, an EC2 Instance, an Elastic Load Balancer, or Route53.
    - **Distribution**: this is the name given the CDN which consists of a collection of Edge Locations.
- CloudFront is discontinuing support for RTMP distributions on December 31, 2020. Now it just provides web distribution.
    1. Services -> CloudFront
    1. Create Distribution
    1. In origin domain name, use your S3 bucket and leave everything as default.
    1. Wait for status changed to Deployed
    1. You can use your domain name of CDN to connect files in S3 (`https://dzwn20yly3er2.cloudfront.net/${FILE_NAME}`).
    1. After finishing, you could disable it and delete it.
- Edge location are not just READ only, you can WRITE to them too.
- Objects are cached for the life of the TTL (Time To Live, always in seconds).
- You can clear cached objects, but you will be charged.

## EC2
- It's a VM service on AWS, there are common following types:
    1. On Demand: allows you to pay a fixed rate by the hour(or second).
    1. Reserved: provides you with a capacity reservation, and offer a significant discount on the hourly charge for an instance. Contract terms are 1 year to 3 year terms.
    1. Spot: Enables you to bid whatever price you want for instance capacity.
    1. Dedicated Hosts: physical EC2 server dedicated for your use.
- EBS (Elastic Block Store): it's a block storage service designed for use with EC2.
    - SSD:
        - General Purpose SSD(GPS): balance price and performance for a wide variety of workloads.
        - Provisioned IOPS SSD(IO1): highest-performance SSD volume for low-latency or high-throughput workloads.
    - Magnetic
        - Throughput Optimized HDD(ST1): low cost HDD volume designed for frequently accessed, throughput-intensive workloads.
        - Cold HDD(SC1): lowest cost HDD volume designed for less frequently accessed workloads(file servers).
- Use EC2
    1. Services -> EC2
    1. Instances -> Click **Launch Instance**
    1. Click **Free tier only** on left panel and choose **Amazon Linux 2 AMI (HVM)**.
    1. Go next (make sure you are choosing free tier type).
    1. Go next (use default setting in **Configure Instance Details**).
    1. Go next (you can change your volume type).
    1. Add some tags to trace your EC2 easier and go next.
    1. In **Configure Security Group** (it's a virtual firewall in the cloud), create a new security group with `WebDMZ` in **Security group name** and **Description**. You need to set up second type HTTP on port 80, and allow it everywhere. For security, you can change **Source** to **My IP** with **SSH** type on port 22 (in 2 rules), then only you can access this EC2. Go next. 
    1. Hit **Launch**, and create a new key pair and download it. After downloading, click **Launch Instances**. Go back to **Instances**, then you'll see your EC2 instance.
    1. Open your terminal, create a working directory and move your key pair file under this folder.
    1. Copy your EC2 instance IP and type following commands:
        ```
        ssh ec2-user@[YOUR_EC2_INSTANCE_IP] -i [YOUR_KEY_PAIR_FILE]
        ```
    1. If you got the following warning, please change the private key file to read only and only for the owner. The command is `chmod 400 myKey.pem`.
        ```
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        It is recommended that your private key files are NOT accessible by others.
        This private key will be ignored.
        ...
        ```
    1. You get in your EC2 instance remotely.
    1. Use `sudo su` to get in as root account.
    1. Type `yum update -y` to update packages.
- Using the CLT
    1. Services -> IAM -> Users
    1. Add user, type your name and click **Programmatic access**. Go next.
    1. Create group -> type your group name and choose policy **AdministratorAccess**.
    1. Add tags.
    1. Create user.
    1. Go to the user you created -> change tab to **Security credentials**.
    1. You can create, make inactive or delete access key here.
    1. Connect to your EC2 again.
        ```
        ssh ec2-user@[YOUR_EC2_INSTANCE_IP] -i [YOUR_KEY_PAIR_FILE]
        ```
    1. Log in as root account:
        ```
        sudo su
        ```
    1. Apply AWS credential. Copy and paste your **Access Key ID** and **Secret Access Key**.
        ```
        aws configure
        ```
    1. The credential is stored in `~/.aws/credential`.
    1. Make a bucket and view all buckets to check if you have right to do.
        ```
        aws s3 mb s3://[YOUR_BUCKET_NAME]
        aws s3 ls
        ```
- Using Roles
    1. Services -> IAM -> Roles
    1. Create role -> Choose EC2
    1. Type **S3** to filter out the policies and click **AmazonS3FullAccess**.
    1. Give it a role name and description.
    1. Create the role.
    1. Go back to EC2 -> Instances -> choose an EC2 instance you created
    1. Actions -> Instance Settings -> Attach/Replace IAM Role
    1. Attach the role you created.
    1. Copy your public EC2 instance.
    1. Go back to your terminal and connect to EC2 as root.
        ```
        ssh ec2-user@[YOUR_EC2_INSTANCE_IP] -i [YOUR_KEY_PAIR_FILE]
        sudo su
        ```
    1. Remove the AWS credential. (if you don't remove it, AWS will try to connect as user instead of role)
        ```
        rm -rf ~/.aws
        ```
    1. Then try to get your bucket info.
        ```
        aws s3 ls
        ```
- Roles:
    - Roles are much more secure than using access key IDs and secret access keys and are easier to manage.
    - You can apply roles to EC2 at any time. When you do this the change takes place immediately.
    - Roles are universal, You don't need to specify what region they are in, similar to user.
- Build a web server
    1. Connect to your EC2 instance by local terminal.
        ```
        ssh ec2-user@[YOUR_EC2_INSTANCE_IP] -i [YOUR_KEY_PAIR_FILE]
        ```
    1. Update packages.
        ```
        sudo su
        yum update -y
        ```
    1. Install Apache.
        ```
        yum install httpd -y
        ```
    1. Start the Apache server.
        ```
        service httpd start
        ```
    1. Go to the root directory of the website. Now there's nothing here.
        ```
        cd /var/www/html
        ```
    1. Create a web page.
        ```
        nano index.html
        ```
    1. Input the page content. When you finished, hit **Ctrl+X** and type **Y** to save. Then hit **Enter** to use original file name.
        ```html
        <html><body><h1>Hello Cloud Gurus, This is WebServer 1</h1></body></html>
        ```
    1. You can open your browser and type your public IP address. Then you can see the content.
- ELB (Elastic Load Balancer)
    1. Services -> EC2 -> Load Balancing -> Load Balancers -> Create Load Balancer.
    1. There are 3 types of ELB, **Classic Load Balancer (ELBv1)**, **Network Load Balancer (ELBv2)** and **Application Load Balancer (ELBv2)**. Now we are going to use Application Load Balancer.
    1. Give ELB name `MyALB`, use HTTP listener as default. Then choose default VPC and click all AZs. Go next.
    1. We don't use HTTPs in this tutorial, so go next.
    1. Select an existing security group `WebDMZ` and go next.
    1. Now we configure routing. We have to create target group, and we will assign it to EC2 instances to use. Give name of target group `MyWebServers`, choose **Instance** type. Then leave the rest of everything default, if you want to change the health check frequency, you can set up in **Advanced health check settings**.
    1. In this step, we can choose EC2 instances to add registered target.
    1. Create the ELB, it could take 5-10 min.
    1. Services -> EC2 -> Instances
    1. Create another instance.
    1. In **Step 3: Configure Instance Details**, choose different subnet compared to first instance. We can type following script in **User data** to launch the instacne with it.
        ```sh
        #!/bin/bash
        yum update -y
        yum install httpd -y
        service httpd start
        chkconfig on
        cd /var/www/html
        echo "<html><body><h1>Hello Cloud Gurus, This is WebServer 2</h1></body></html>" > index.html
        ```
    1. Services -> EC2 -> Load Balancing -> Target Groups
    1. Choose `MyWebServers` and click Targets tab. Then add the second EC2 instance to it. After adding it, you could spent some time to wait the status changing to healthy.
    1. Services -> EC2 -> Load Balancing -> Load Balancers
    1. Copy the DNS name and paste it to your browser then search it. You will find the content is changing every time you refresh the page, that's ELB effect.
    1. If you try to terminate one of 2 EC2 instances, it could be a little down time. When you visit DNS after few seconds, everything works again.

## Database
- Relational databases on AWS is called **RDS**. RDS includes different types of database as follows:
    - SQL Server
    - Oracle
    - MySQL Server
    - PostgreSQL
    - Aurora
    - MariaDB
- RDS has 2 key features:
    - Multi-AZ: for disaster recovery.
    - Read Replicas: for performance.
- Amazon's NoSQL database is called **DynamoDB**.
- Warehousing: used to pull in very large and complex datasets. Usually used by management to do queries on data. Tools like Cognos, Jaspersoft, SQL Server Reporting Services and so on. Amazon's data warehouse solution is called **RedShift**.
- ElastiCache is a web service that makes it easy to deploy and operate an in-memory cache in the cloud. ElastiCache supports 2 open-source in-memory caching engines:
    - Memcached
    - Redis
- Lab for provision an RDS instance.
    1. Services -> EC2 -> Instances
    1. Create an EC2 instance with following options:
        - Set up **User data** in **Advanced Details** (in **Configuration Instance** tab) as follows:
            ```sh
            #!/bin/bash
            yum install httpd php php-mysql -y
            amazon-linux-extras install -y php7.2
            cd /var/www/html
            wget https://wordpress.org/wordpress-5.4.1.tar.gz
            tar -xzf wordpress-5.4.1.tar.gz
            cp -r wordpress/* /var/www/html/
            rm -rf wordpress
            rm -rf wordpress-5.4.1.tar.gz
            chmod -R 755 wp-content
            chown -R apache:apache wp-content
            service httpd start
            chkconfig httpd on
            ```
        - Add a tag with key `Name` and value `WebServer01`.
        - Create a new security group named `web-dmz`, and add 2 rules. One is SSH type with **Anywhere** source, another is HTTP type with **Anywhere** source as well.
        - Then you can create or choose existing key pair to create this instnace.
    1. Services -> RDS -> Databases
    1. Create database with following options:
        - Standard Create
        - MySQL
        - Template: Free tier
        - DB instance identifier: `mydb`
        - Set up your master username and master password (don't need to auto generate a password)
        - DB instance size -> Burstable classes: **db.t2.micro**. 
        - In additional configuration, type `mydb` for initial database name.
    1. Go back to Databases and click **mydb**, you can see a lot of informations about this database here. Now under the **Connectivity & security** tab, click the **VPC security groups** link. We need to open a port for SQL traffic.
    1. Switch to **InBound rules** tab and edit it. Add a new rule, choose **MYSQL/Aurora** type, and select the **web-dmz** security group we created in previous step. Then save rules. Our RDS is using this default SG, this setting allows **web-dmz** SG can visit RDS, which means **WebServer01** we created can visit this RDS.
    1. Go to your EC2 instance to copy the public IP and visit this IP, you should see **Wordpress** page. Input the database name `mydb`, username and password. For the database host, check your endpoint of your RDS instance. You can go to RDS Databases, click on **mydb** and see it in **Connectivity & Security** tab. Then submit.
    1. Now the page will tell you "Unable to write to `wp-config.php` file", do the following steps and then click **Run the installation**.
        1. Go to your EC2 instance to copy the public IP. Then open your terminal and connect to EC2.
            ```
            ssh ec2-user@[YOUR_EC2_INSTANCE_IP] -i [YOUR_KEY_PAIR_FILE]
            ```
        1. Create a file called `wp-config.php`, then copy and paste the content on Wordpress page or [here](wp-config.php).
            ```
            sudo su
            cd /var/www/html
            nano wp-config.php
            ```
        1. Make sure that you paste it successfully.
    1. Go back to Wordpress, set up your **Site Title** and other informations. Then click on **Install Wordpress**.
    1. Log in with your username and password.
    1. Go to Settings, **WordPress Address (URL)** and **Site Address (URL)** are same with your EC2 public IP. But if we restart the instance, the IP will be changed, so we should use domain name provided by ELB to be our URL.
    1. Please follow the steps in previous section to create an ELB and registry **WebServer01** to registered targets.
    1. Go to EC2 -> Load Balancing -> Load Balancers, copy your ELB's DNS name and paste it to **WordPress Address (URL)** and **Site Address (URL)** in Wordpress settings (don't forget to add prefix `http://` before DNS).
    1. Now you can use the DNS as your Wordpress website, please try to create a new post and see results.
    1. You can create an image for this EC2 instance for future use.
        1. EC2 -> Instacnes -> Instances
        1. Choose **WebServer01** and click Actions -> Image -> Create Image.
        1. Input image name `MyWordpressTemplate` and create image.
        1. You can check this image in EC2 -> Images -> AMIs.
- Autoscaling
    1. EC2 -> Instances -> Instances -> terminate your all EC2 instances
    1. EC2 -> Auto Scaling -> Launch Configurations -> Create launch configuration with following options:
        - Name it as `myLCG`.
        - Choose your AMI **MyWordpressTemplate**.
        - Choose **t2.micro** for your instance type.
        - In Advanced details, input following script for your **User data**.
            ```sh
            #!/bin/bash
            yum update -y
            ```
        - Select an existing security group **web-dmz**.
        - Choose an existing key pair your created before.
    1. EC2 -> Auto Scaling -> Auto Scaling Groups -> Create Auto Scaling group with following options:
        - Give Auto Scaling group name `myASG`.
        - Switch to launch configuration and choose your launch configuraion **MyLCG**.
        - Choose all subnets.
        - In **Configure advanced options**, click **Enable load balancing** and choose **Application Load Balancer or Network Load Balancer**. Then choose a target group **MyWebServers** you created. For **Health checks**, please click **ELB**.
        - In **Configure group size and scaling policies**, set up desired capacity and minimum capacity `2`, and maximum capacity is `3`. Choose **Target tracking scaling policy**, using Target tracking policy -> Average CPU utilization, target value is `90`.
        - Leave other settings default and create.
    1. Go back to EC2 instances, you will see 2 EC2 instances are creating. Now check your Load Balancing -> Target Groups, you will find 2 instances are registered.
    1. Wait for EC2 running, then you can go to ELB to find DNS name of **MyALB**. Visit this URL and you can see your Wordpress.
## Route 53
- Domain name
    1. Services -> Route 53 -> Domains -> Registered domains -> Click button "Register Domain"
    1. Type your domain name, e.g. `fakedomain` and hit **Check** to check.
    1. Go to S3 bucket and create a new bucket named `fakedomain` and allow this bucket to be public. Click `fakedomain` bucket and do following actions.
        - Switch to **Properties** tab, then click **Static website hosting** and choose **Use this bucket to host a website**. For index and error document, type `index.html` and `error.html`, then click save.
        - Switch to **Permissions** tab and choose **Bucket Policy**, and copy the following JSON and paste on it. Remember replace `BUCKET_NAME` by your bucket name (e.g. `arn:aws:s3:::fakedomain/*`).
            ```json
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "PublicReadGetObject",
                        "Effect": "Allow",
                        "Principal": "*",
                        "Action": [
                            "s3:GetObject"
                        ],
                        "Resource": [
                            "arn:aws:s3:::BUCKET_NAME/*"
                        ]
                    }
                ]
            }
            ```
        - Upload file [index.html](index.html). and [error.html](error.html).
    1. Go back to Route 53, if there's no error after **Check**, then you can add to cart. Input some data and wait for some days. Then you get your domain.
    1. Click your domain and click **Create Record Set**.
        - Choose **A - IPv4 address** for **Type**.
        - For **Alias**, choose **Yes** and target is your S3 bucket you created.
        - Then save.
    1. Visit your domain (e.g. http://fakedomain.com) and you should see the content of `index.html`. Try to visit http://fakedomain.com/abc then you will see the error page.

## Elastic Beanstalk
With Elastic Beanstalk, you can quickly deploy and manage application in the AWS Cloud. You simply upload your application, and Elastic Beanstalk automatically handles the details of capacity provisioning, load balancing scaling, and application health monitoring.
1. Services -> Elastic Beanstalk -> Create a web app
    - Application name: `HelloApp`.
    - Platform: `PHP`.
    - Application code: choose **Sample Application**.
1. Click **Application** in left panel, then click `HelloApp`.
1. Click URL and you will see the sample application.

## CloudFormation
- You create a template that describes all the AWS resources that you want, and AWS CloudFormation takes care of provisioning and configuration those resources for you. You don't need to individually create and configure AWS resources and figure out what's dependent on what.
- Notice that Elastic Beanstalk and CloudFormation are both **FREE** services, however the resources they provision are ***NOT*** free.
- Elastic Beanstalk is limited in what it can provision and is not programmable. CloudFormation can provision almost any AWS services and is completely programmable.
- Example:
    1. Services -> CloudFormation -> Create stack
        - Prepare template: choose **Use a sample template**.
        - Sample templates: choose **WordPress blog**.
        - Stack name: `myFirstStack`.
        - Use `myFirstStack` for DBName, DBPassword, DBRootPassword and DBUser. InstanceType choose **t2.micro**.
        - Leave the rest of option default and create stack.
    1. Click `myFirstStack` and choose **Outputs** tab, click on WebsiteURL to see this example. But there's an issue here that AWS doesn't match the version of PHP and Wordpress, so you might get following error:
        ```
        Your server is running PHP version 5.3.29 but WordPress 5.4.2 requires at least 5.6.20.
        ```
    1. Click `myFirstStack` and choose **Template** tab, click **View in Designer**. Take a look of this Stack structure.
    1. Check your EC2, you can find CloudFormation create a EC2 instance for you.
    1. If you want to delete this stack, you should delete it from CloudFormation instead of EC2.

## CloudWatch
CloudWatch is a monitoring service to monitor your AWS resources and applications. For example, CloudWatch with EC2 will monitor events every 5 minutes by default, you can monitor compute, storage, CPU and so on. You can create CloudWatch alarms which trigger notifications. Remember, CloudWatch is all about performance.

## Systems Manager
AWS Systems Manager allows you to manage your EC2 instances at scale. For example, if you have a lot of EC2 instances, which is so-called **EC2 Fleet**. You want to install package across all EC2 instances, you can run command on Systems Manager. Systems Manager will run that command on all EC2 instances via pre-installed software on those EC2 instances. In face, Systems Manager can be used to manager fleets of EC2 instacnes and virtual machines, so it can be both inside AWS and on-premise.

## Pricing
Please refer to [How AWS Pricing Works](https://d0.awsstatic.com/whitepapers/aws_pricing_overview.pdf).

- **Capex** stands for Capital Expenditure which where you pay up front. It's a fixed, sunk cost.
- **Opex** stands for Operational Expenditure which is where you pay for what you use.

### Which services are free?
- VPC
- Elastic Beanstalk
- CloudFormation
- IAM
- Auto Scaling
- Opsworks
- Consolidated Billing

### Budgets
- Click your account name on top-right bar -> **Cost Management** on left panel -> **Budgets**
- Budgets is used to budget (or predict) costs **BEFORE** they are incurred.

### Cost Explorer
- Click your account name on top-right bar -> **Cost Management** on left panel -> **Cost Explorer**
- Cost Explorer is used to explore costs **AFTER** they have been incurred.

### Resource Groups & Tags
- Tags are key value pairs attached to AWS resources. 
- Resource groups make it easy to group your resources using tags. You can group resources that share one or more tags. Resource groups contain information such as:
    - Region
    - Name
    - Employee ID
    - Department
    - Some other specific information (e.g. EC2 has public and private IP address)
- Lab
    1. Create a EC2 instance with following tags:
        key|value
        --|--
        Name|WebServer01
        Department|Finance
        Region|Virginia
        EmployeeID|12345
    1. Resource Groups -> Tag Editor
    1. In **Find resources to tag** section, select **All regions**, **All resource types**. For tags, type key `Department` and you don't need to type value, then click **Search resources**.
    1. You can see the EC2 you created, then you can edit tags here.
    1. Resource Groups -> Create a group
        - You need to change to the region same with EC2 you created.
        - For tags, type key `Department` and you don't need to type value, then click **Add**.
        - Input group name `Department`.
        - Create group.
    1. Services -> Systems Manager -> Application Management -> Resource Groups -> Click `Department`.
    1. Now you can use Systems Manager to close all EC2 instances in this resource group. Click **Execute automation** and choose **AWS-StopEC2Instance**.
    1. For **Parameter** in **Target**, you should choose **InstanceId**. Leave everything else default and click **Execute**.
    1. Go back to EC2 dashboard to check EC2 is stopped.

### Organizations
- AWS Organizations is an account management service that enables you to consolidate multiple AWS accounts into an organization. AWS Organizations has 2 feature sets: consolidated billing and all features.
    <img src="screenshots/organization.png" alt="organization"/>
- We can use a paying account to handle payment. Notice that you cannot access resources of the other accounts from paying account. This consolidated billing model can provide you one bill per AWS account, and it's very easy to track charges and allocate costs. You will get volume pricing discount as well. 
    <img src="screenshots/paying-account.png" alt="paying-account"/>

- Some best practice with AWS organizations:
    - Always enable multi-factor authentication on root account.
    - Always use a strong and complex password on root account.
    - Paying account should be used for billing purposes only. **Do not deploy resources into the paying account**.
- You can link 20 accounts only. 
- When monitoring is enabled on the paying account, the billing data for all linked accounts is included.
- You can still create billing alerts per individual account.
- We can use CloudTrail with AWS Organizations. CloudTrail is on a per account and per region basis, but can be aggregated into a single bucket belonging to the paying account.
    - CloudTrail is enabled per region and per AWS account.
    - CloudTrail can consolidate logs an S3 bucket:
        1. Turn on CloudTrail in paying account.
        1. Create a bucket policy that allows cross-account access.
        1. Turn on CloudTrail in the other accounts and use the bucket in the paying account.
- Organization Lab
    1. Click **My Organization** on the top panel.
    1. Create organization.
    1. Add account -> invite account (invite an existing AWS account to join your organization).
    1. Switch to tab **Organize accounts** -> **New organizational unit** -> type the organizational unit name, e.g. `Developers`. -> create unit.
    1. Switch to tab **Policies** -> click **Service control policies** -> enable it -> create policy.
        - Policy name: `GoServerless`
        - Description: `Policy blocks EC2`
        - Policy:
            ```json
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "Statement1",
                        "Effect": "Deny",
                        "Action": [
                            "ec2:*"
                        ],
                        "Resource": ["*"]
                    }
                ]
            }
            ```
    1. Go back to tab **Organize accounts**, choose an accnount and move it to **Developers**.
    1. Click **Developers**, and click **Service control policies** 
on the right panel. Find **GoServerless** then click **Attach**. Now all accounts under **Developers** cannot access to EC2.

### CloudTrail
- CloudWatch monitors performance.
- CloudTrail monitors API calls in the AWS platform.

### Quick Start
Quick Starts is a way of deploying environments quickly, using CloudFormation templates built by AWS Solutions Architects who are experts in that particular technology. Visit [AWS Quick Start](https://aws.amazon.com/quickstart/?quickstart-all.sort-by=item.additionalFields.sortDate&quickstart-all.sort-order=desc).

### AWS Landing Zone
AWS Landing Zone is a solution that helps customers more quickly set up a secure, multi-account AWS environment based on AWS best practices. Visit [AWS Landing Zone](https://aws.amazon.com/solutions/implementations/aws-landing-zone/).

### AWS Calculators
AWS helps you calculate your costs using a couple of different calculators.
- [Simple Monthly Calculator](https://calculator.s3.amazonaws.com/index.html): It's used to calculate your running costs on AWS on a per month basis. It's not a comparison tool.
- [AWS Total Cost of Ownership Calculator](https://aws.amazon.com/tco-calculator/)(AWS TCO calculator): It's used to compare costs of running your infrastructure on premise vs in the AWS Cloud. It will generate reports that you can give to your C-level executives to make a business case to move to the cloud.

## Security

### Compliance
The AWS Compliance Program helps customers to understand the robust controls in place at AWS to maintain security and compliance in the cloud. Please refer to [AWS Compliance Programs](https://aws.amazon.com/compliance/programs/). You can also read the Report in [AWS Artifact](https://console.aws.amazon.com/artifact/reports).

### AWS Artifact
AWS Artifact is used to retrieve compliance reports.

### Shared Responsibility Model
While AWS manages security of the cloud, security in the cloud is the responsibility of the customer. You should read the [official document](https://aws.amazon.com/compliance/shared-responsibility-model/) before going to test. 

If you can do this in the AWS console, you are likely responsible. If not, AWS are likely responsible. Notice that Encryption is a shared responsibility.

<img src="screenshots/shared-responsibility-model.jpg" alt="shared-responsibility-model"/>

### AWS WAF & Shield
- AWS WAF: WAF is a web application firewall, designed to stop hackers.
- AWS Shield: It's a DDoS mitigation service designed to stop DDoS attacks.

### AWS Inspector, Trusted Advisor, CloudTrail
- AWS Inspector: It's used for inspecting EC2 instances for vulnerabilities.
- AWS Trusted Advisor: It inspects your AWS accounts as a whole (not just EC2). It does more than just security checks. It also does Cost Optimization, Performance and Fault Tolerance.
- AWS CloudTrail: It increase visibility into your user and resource activity by recording AWS Management Console actions and API calls.

### AWS Condfig
- CloudWatch is used for monitoring performance.
- AWS Config is used to monitor configurations of your AWS resources.

### Athena vs Macie
- Athena is an interactive query service which enables you to analyze and query data located in S3 using standard SQL.
    - Athena can be used to query log files stored in S3 (e.g. ELB logs, S3 access logs).
    - Generate business reports on data stored in S3.
    - Analyze AWS cost and usage reports.
    - Run queries on click-stream data.
- Macie is a security service which uses ML and NLP to discover, classify and protect sensitive data stored in S3.
    - Macie uses AI to analyze data in S3 and helps identify PII.
    - Macie can also be used to analyze CloudTrail logs for suspicious API activity.
    - Includes Dashboards, Reports and Alerting.

## Hints
### Which AWS services are global?
- IAM
- Route 53
- CloudFront
- SNS (available in all regions)
- SES (not available in all regions)
### Which services give global views but are regional?
- S3
### Whick services can be used on-premises?
- Snowball
- Snowball Edge
- Storage Gateway
- CodeDeploy
- Opsworks
- IoT Greengrass
### Which AWS services can be used to deploy applications on-premises?
- CodeDeploy
- Opsworks
