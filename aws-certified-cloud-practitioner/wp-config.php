<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mydb' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'mydb.cua7v2muuvzv.us-east-1.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xj8j!0Bezl<BJ]amgMAXR:Dd#W=lZc^5ZV)**Qo kd>{IxjQJe)Mr_-)bSm>W<)2' );
define( 'SECURE_AUTH_KEY',  'rP|sJNXk:fi.)^Ye$/#iWm,Up!$S t9^FKZzusEa[pVl/~xjv#g;*I}to*aG1z6L' );
define( 'LOGGED_IN_KEY',    'aB5LD5!T_.YVQxOOIZX*Tvf^| (~yHbQq>qi>sk8J5+80oqK8dCH22r^hg!E~:l+' );
define( 'NONCE_KEY',        'Nx|ushWrV8<{agFKc7.*b-o?edLw/9OaWQvI#S4G$Gqtj=Rd5XhoChai$Orm5%UR' );
define( 'AUTH_SALT',        'v7|s}e#v{nX{CN6^KZVKfM#1&flV$m{(k)O9Y/=%<S?pS_j+omRl4V@ht%Td.;Pm' );
define( 'SECURE_AUTH_SALT', 'KC3KAAqkIU,SFE,NnJj5eG:+|p0/}J-W:o#j6utqW3ony&+/%}2ZU-^WBroKO$@Y' );
define( 'LOGGED_IN_SALT',   'F?x>8?PWrf9zL/CTlX}_U~^626Wq:{d*5aD<.s<l!DR8}4kO%R6kK6NVWmY&5?tt' );
define( 'NONCE_SALT',       'H_|tuyIl0k;MK(&l v,~]}9GEIW$w&7Xo `)_%?[2YhGzgOf6A_<I|+#RS$kO}x-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
