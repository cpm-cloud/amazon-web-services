# AWS Certified Developer - Associate

## IAM
Identity Access Management (IAM) is a **global** service to allow you manage users and their level of access to the AWS services. It has following features:
- Centralized control of the AWS account.
- Shared access to the AWS account.
- Granular premissions.
- Identity Federation (including Active Directory, Facebook, Linkedin etc.)
- Multifactor authentication.
- Provide temporary access for users/devices and services where necessary.
- Allows you to set up your own password rotation policy.
- Supports PCI DSS Compliance.

IAM has following critical concepts:
- **Users**: End users such as people, employees of an organization etc.
- **Groups**: A collection of users. Each user in the group will inherit the permissions of the group.
- **Roles**: They are assigned to AWS Resources.
- **Policies**: They are made up of documents, called Policy documents. These documents are in a JSON format and they give permissions as to what a Users/Group/Role is able to do.

There are 3 types of IAM policies:
- Managed policy (recommended): It's default policy managed by AWS.
- Customer Managed Policy: It's managed by you.
- Inline Policy: It's managed by you and embedded in a single user, group or role. There is a strict 1-1 relationship, when you delete that user, group or role, the policy will be deleted.

New users have ***NO*** permissions when first created, you have to use policies to give them permissions. But new users are assigned **Access Key ID** and **Secret Access Keys** when first created. These are not the same as a password, you cannot use them to login in to the console, you can use them to access AWS via the APIs and command line, however. Keep in mind that you can view these keys once. If you lose them, you have to regenerate them.

In practical scenarios, here are some tips:
- **Least privilege**: Always give your users the minimum amount of access required.
- Create groups: Assign your users to groups so that they will automatically inherit the permissions of the group.
- **Do not use just one access key**. Don't create just one access key and share that with all your developers. You should create one key pair per developer.
- Roles allow you to not use **Access Key ID**s and **Secret Access Key**s. So they are preferred from a security perspective.

Remember testing IAM permissions before you commit them to production. You can access the IAM Policy Simulator Console [here](https://policysim.aws.amazon.com/).

**IAM Access Analyzer** helps you identify the resources in your organization and accounts, such as Amazon S3 buckets or IAM roles, that are shared with an external entity. This lets you identify unintended access to your resources and data, which is a security risk.

**Access Advisor feature on IAM console** helps identify the unused roles, IAM reports the last-used timestamp that represents when a role was last used to make an AWS request. Your security team can use this information to identify, analyze, and then confidently remove unused roles.

### IAM Policy Types
IAM policies define permissions (grant or limit) for an action regardless of the method that you use to perform the operation. AWS supports 6 types of policies:

- *Identity-based policies*: It attach managed and inline policies to IAM identities (users, groups to which users belong, or roles). Identity-based policies grant permissions to an identity.
- *Resource-based policies*: It attach inline policies to AWS resources. The most common examples are S3 bucket policies and IAM role *trust policies* (specifies which trusted account members are allowed to assume the role). These policies grant the specified principal permission to perform specific actions on that resource and defines under what conditions this applies. Principals can be in the same account as the resource or in other accounts.
- *Permissions boundaries* – Use a managed policy as the permissions boundary for an IAM entity (user or role). That policy defines the maximum permissions that the identity-based policies can grant to an entity, but **does not grant permissions**. Permissions boundaries do not define the maximum permissions that a resource-based policy can grant to an entity.
- Organizations SCPs – Use an AWS Organizations service control policy (SCP) to define the maximum permissions for account members of an organization or organizational unit (OU). SCPs limit permissions that identity-based policies or resource-based policies grant to entities (users or roles) within the account, **but do NOT grant permissions**.
- Access control lists (ACLs) – Use ACLs to control which principals in other accounts can access the resource to which the ACL is attached. ACLs are similar to resource-based policies, although **they are the ONLY policy type that does not use the JSON policy document structure**. ACLs are cross-account permissions policies that grant permissions to the specified principal. ACLs cannot grant permissions to entities within the same account.
- Session policies – Pass advanced session policies when you use the AWS CLI or AWS API to assume a role or a federated user. Session policies limit the permissions that the role or user's identity-based policies grant to the session. Session policies limit permissions for a created session, **but do not grant permissions**.

### Web Identity Federation
Web Identity Federation allows user to authenticate with a web ID provider (Google, Facebook, Amazon). The user authenticates first with the web ID provider and receives an authentication token, which is exchanged with AWS Cognito for temporary AWS credentials allowing them to assume an IAM role. Cognito uses push synchronization to send a silent push notification of user data updates to multiple device types associated with a user ID.

<img src="screenshots/cognito.png" alt="cognito"/>

AWS Cognito is an Identity Broker handling interaction between your applications and the web ID provider. It has following features:
- It provides sign-up, sign-in and guest user access.
- It syncs user data for a seamless experience across your devices.
- Cognito is the AWS recommended approach for web ID Federation particularly for mobile apps.

Amazon Cognito has 2 main components:
- **User pools**: They are user directories providing sign-up and sign-in options for your app users. User can sign-in directly to the user pool or indirectly via an ID provider. Successful authentication generates a number of JSON web tokens (JWTs).
- **Identity pools**: They enable you to grant your users access to other AWS services. Identity pool is where to exchange the user pool tokens for AWS credentials.

<img src="screenshots/user-pool.png" alt="user-pool"/>

### Cognito Lab
1. Services -> Cognito -> Manage User Pools -> Create a user pool:
    - Pool name: `myuserpool`.
    - Click **Review defaults**.
    - Create pool.
1. Choose your user pool -> App clients -> Add an app client with named `myClient`.
1. Choose your user pool -> App client settings:
    - Enabled Identity Providers: check **Cognito User Pool**.
    - Callback URL(s): `https://example.com`.
    - Allowed OAuth Flows: check **Authorization code grant** and **Implicit grant**.
    - Allowed OAuth Scopes: select all.
1. Choose your user pool -> Domain name, please type any string as your domain name (global unique).
1. Choose your user pool -> App integration, you can see your domain like `https://acgdomain.auth.us-west-2.amazoncognito.com`. Visit following URL, remember to replace `[YOUR_COGNITO_DOMAIN]` with your domain. For `[YOUR_APP_CLIENT_ID]`, you can get it at App client settings (it's ID's value) like `3aihss4l8l7l184cb8go26hd9h`.
    ```
    [YOUR_COGNITO_DOMAIN]/login?response_type=token&client_id=[YOUR_APP_CLIENT_ID]&redirect_uri=https://example.com
    ```
1. You should see the page like:
    <img src="screenshots/sign-in.png" alt="sign-in"/>
1. Try to create a user and sign up.
1. After signing up, you can go back to Cognito console. Choose your user pool and select **User and groups**, you should see the user you created.
1. Now switch to **Groups** tab, you can create a group with IAM role. Then add user you created to this group, which means the token of users who are belong to this groups can access AWS resources as that IAM role.

### STS - AssumeRoleWithWebIdentity
**AssumeRoleWithWebIdentity** is an API provided by STS (Security Token Service). It returns temporary security credentials for users authenticated by a mobile or web application or using a web ID provider. Notice that `AssumedRoleUser` object (includes `ARN` and `AssumedRoleId` properties) is used to programmatically reference the temporary credentials instead of an IAM role or user.
- For mobile applications, Cognito is recommended.
- For web applications, you can use `AssumeRoleWithWebIdentity` API.

### Cross Account Access
You can delegate access to resources across AWS accounts that you own, which means you can share resources in one account with users in a different account. All you need is to create a **role** in one account to allow access and and grant permissions to users in a different account. You also can switch roles within the AWS managed console.

You can follow this official [tutorial](https://docs.aws.amazon.com/IAM/latest/UserGuide/tutorial_cross-account-with-roles.html) to practice.

## EC2
Elastic Compute Cloud (EC2) is VM hosted in AWS. There are different pricing models:

- On-Demand: Pay by the hour or the second depending on the type of instance you run.
- Reserved (regional): Reserved capacity for 1 or 3 year. Up to 72% discount on the hourly charge.
- Spot: Purchase unused capacity at a discount of up to 90%. Prices fluctuate with supply and demand.
- Dedicated: A physical EC2 server dedicated for you use. It's the most expensive option.

Pricing Options|Use Cases
--|--
On-Demand|Applications with short-term, spiky, or unpredictable workloads.<br>Applications being developed or tested on Amazon EC2 for the first time.
Reserved|Applications with steady state or predictable usage.<br>Customers that can commit to using EC2 over a 1 or 3 year term to reduce their total computing costs.
Spot|Applications that have flexible start and end times.<br>Users with urgent computing needs for large amounts of additional capacity.
Dedicated|Great for server-bound licenses to reuse or compliance requirements.

EC2 instance type determines the hardware of the host computer. There are many EC2 instance types, you can refer to official [document](https://aws.amazon.com/ec2/instance-types/). Each instance type offers different compute, memory and storage capabilities. They are grouped in instance families.

For launching an EC2 instance lab, please refer to [here](../../level-1/aws-certified-cloud-practitioner/README.md#ec2).

### EBS
Elastic Block Store (EBS) is virtual disks that you can attach to the EC2 instances. You can create a file system, run a database, or use them in any other way you would use any system disk. You can encrypt EBS volume when creating, but there's not direct way to encrypt existing EBS volume, you can encrypt it by creating either a volume or a snapshot. 

There are following types of EBS, you get can more details [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html).
- General Purpose SSD (gp2): Balance price and performance.
- Provisioned IOPS SSD (io1): High performance SSD for low latency or high-throughput workloads.
- Provisioned IOPS SSD (io2): Latest generation with higher durability and more IOPS.
- Throughput Optimized HDD (st1): Designed for frequently accessed, throughput-intensive workloads.
- Cold HDD (sc1): Designed for less frequently accessed workloads.

**IOPS** (Input/Output Operations Per Second) is an important metric for quick transactions, low latency apps and transactional workloads. IOPS measures the number of read and write operations per second.

**Throughput** measures the number of bits read or written per second. It's an important metric for large datasets, large I/O sizes and complex queries. Throughput is the ability to deal with large datasets.

It's ***NOT*** possible to directly share an encrypted EBS volume with another AWS account. Instead, create and share an encrypted EBS snapshot with the destination AWS account. Then, create a new EBS volume from a copy of the shared snapshot.

### ELB
AWS Elastic Load Balancers (ELB) have 3 different types:
- **Application Load Balancer (ELBv2)**: It's best suited for HTTP and HTTPs traffic. They operate at Layer 7 (OSI). You can create advanced request routing, sending specified requests to specific web services.
- **Network Load Balancer (ELBv2)**: It's best suited for TCP traffic. Operating at the connection level (Layer 4), network load balancers are capable for handling millions of requests per second.
- **Classic Load Balancer (ELBv1)** : It's the legacy, you can load balance HTTP/HTTPs applications and use Layer 7-specific features, such as `X-Forwarded-For` (you can get public IP of end user from this property value) and sticky sessions. You can also use strict Layer 4 load balancing for applications reling on TCP. If your application stops responding, the ELB responds with a 504 error.

If you want to analyze the incoming requests for latencies and the client's IP address patterns, you should use *access logs*. ELB provides access logs that capture detailed information about requests sent to your load balancer. Each log contains information such as the time the request was received, the client's IP address, latencies, request paths, and server responses. You can use these access logs to analyze traffic patterns and troubleshoot issues.

*Sticky sessions* are a mechanism to route requests to the same target in a target group. When the load balancer receives a request from a client that contains the cookie, if sticky sessions are enabled for the target group and the request goes to the same target group, the load balancer detects the cookie and routes the request to the same target. If the cookie is present but cannot be decoded, or if it refers to a target that was deregistered or is unhealthy, the load balancer selects a new target and updates the cookie with information about the new target.

### EC2 Auto Scaling
Amazon EC2 Auto Scaling helps you ensure that you have the correct number of Amazon EC2 instances available to handle the load for your application. You create collections of EC2 instances, called *Auto Scaling groups*.

To maintain the same number of instances, EC2 Auto Scaling performs a periodic health check on running instances within an Auto Scaling group. When it finds that an instance is unhealthy, it terminates that instance and launches a new one. If you stop or terminate a running instance, the instance is considered to be unhealthy and is replaced. 

### AWS CLI
The AWS Command Line Interface (AWS CLI) is a tool that enables you to interact with AWS services using commands in your command-line shell. You can install CLI on your local machine, please refer to [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html).

Before conneting to AWS services, you need to set up configuration. Please see document [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).

Be default, AWS CLI uses a page size of 1,000. If you see errors like "time out" when running list commands on a large number of resources, the default page size is too large. You can use `--page-size` or `--max-items` (only return first n items) to fix it.

### Route 53
Route 53 is Amazon's DNS service. It allows you to map your domain names to following AWS resources. You can see the lab [here](../../level-1/aws-certified-cloud-practitioner/README.md#ec2).
- EC2 instance
- Load Balancers
- S3 Buckets

Route 53 supports the DNS record types:
- A record: You use an A record to point a domain or subdomain to an IPv4 address.
- AAAA record: You use an AAAA record to point a domain or subdomain to an IPv6 address.
- CNAME record: A CNAME record maps DNS queries for the name of the current record, such as acme.example.com, to another domain (example.com or example.net) or subdomain (acme.example.com or zenith.example.org).
- PTR record: A PTR record maps an IP address to the corresponding domain name.
- Alias record: Alias records let you route traffic to selected AWS resources, such as CloudFront distributions and Amazon S3 buckets. For example, you can create an alias record named acme.example.com that redirects queries to an Amazon S3 bucket that is also named acme.example.com. You can also create an acme.example.com alias record that redirects queries to a record named zenith.example.com in the example.com hosted zone.

### EC2 with User Lab
1. Create an EC2 instance with default setting.
1. Open your terminal and connect to your EC2 instance.
    ```
    ssh -i keyPair.pem ec2-user@12.123.123.123
    ```
1. Create a IAM user with **Programmatic access** access policy. Copy **Access key ID** and **Secret access key**, you need to use them later.
1. Create a IAM group named `Developers` with **AdministratorAccess** policy.
1. Add user you created to group **Developers**.
1. Set up AWS configuration. Paste your **Access key ID** and **Secret access key**.
    ```
    sudo su
    aws configure
    ```
1. Now you can try S3 commands. First of all, let's make a S3 bucket and check it out.
    ```
    aws s3 mb s3://[YOUR_BUCKET_NAME]
    aws s3 ls
    ```
1. Make a file and copy to this bucket.
    ```
    echo "Hello, I'm using CLI" > hello.txt
    aws s3 cp hello.txt s3://[YOUR_BUCKET_NAME]
    aws s3 ls s3://[YOUR_BUCKET_NAME]
    ```

### EC2 with S3 Role Lab
1. Create an EC2 instance with default setting.
1. Open your terminal and connect to your EC2 instance.
    ```
    ssh -i keyPair.pem ec2-user@12.123.123.123
    ```
1. Create an IAM Role `MyS3AdminAccess` with **AmazonS3FullAccess** policy.
1. Choose your instance -> Actions -> Instance Settings -> Attach/Replace IAM Role -> choose a Role you created and **Apply**.
1. Then you can try to get S3. If you have connected to this EC2 by IAM user before, you will still get an error message. That's because the keys still in this EC2. Go to following folder, you'll see credentials still there.
    ```
    ls ~/.aws
    ```
1. Remove the credentials and config (Use `y` to confirm).
    ```
    rm ~/.aws/credentials
    rm ~/.aws/config
    ```
1. Now you can access to your S3 bucket.
    ```
    aws s3 ls
    ```

## RDS
Relational Database Service (RDS) is a relational database service in cloud. RDS is **OLTP** (Online Transaction Processing) which is designed to serve as a persistent state store for front-end applications. e.g. Aurora, MySQL, PostgreSQL etc. For RDS lab, please refer to [here](../../level-1/aws-certified-cloud-practitioner/README.md#database).

**OLAP** (Online Analytics Processing) is an database designed to process large datasets quickly to answer questions about data. e.g. RedShift.

**Data Warehousing** is used to pull in very large and complex datasets. Usually used by management to do queries on data (e.g. current performance vs targets).

**AWS Secrets Manager** has built-in integration for RDS databases. It is a service that can be used to securely store, retrieve, and automatically rotate database credentials. Notice that **Systems Manager Parameter Store** provides secure storage of sensitive information as well. However, it does not provide automatic credentials rotation capability.

### Backups
There are 2 types of backups for AWS:
- **Automated Backups** (default): They allow you to recover your database to any point in time within a *retention period*. The retention period can be between 1 and 35 days. Automated Backups will take a full daily snapshot and will also store transaction logs throughout the day. The backup data is stored in S3 and you get free storage space equal to the size of your database.
- **Snapshots**: DB snapshots are done manually (they are user initiated). They are stored even after you delete the original RDS instance, unlike automated backups.

Whenever you restore either an Automatic Backup or a mannual Snapshot, the restored version of the database will be a new RDS instance with a new DNS endpoint.

### Encryption
Encryption at rest is supported. But encrypting an existing DB instance is not supported. To use Amazon RDS encryption for an existing database, you must first create a snapshot, make a copy of that snapshot and encrypt the copy.

### Multi-AZ
Multi-AZ allows you to have an exact copy of your production database in another AZ (Available Zone). The data will automatically be synchronized to the stand by database. If DB instance failure, RDS will automatically failover to the standby so that DB operations can resume quickly. Multi-AZ is for disaster recovery only, it's not used for improving performance. For performance improvement, you need **Read Replicas**.

If the Amazon RDS instance is configured for Multi-AZ, you can perform the reboot with a failover. When you force a failover of your DB instance, Amazon RDS automatically switches to a standby replica in another Availability Zone, and updates the DNS record for the DB instance to point to the standby DB instance.

Rebooting with failover is beneficial when you want to simulate a failure of a DB instance for testing, or restore operations to the original AZ after a failover occurs.

### Read Replicas
Read Replicas allow you to have a read-only copy of your production DB. This is achieved by using Asynchronous replication from the primary RDS instance to the read replica. You use read replicas for very read-heavy DB workloads. Read Replica have following features:
- Must have automatic backups turned on in order to deploy a read replica.
- You can have up to 5 read replica copies of any DB.
- You can have read replicas of read replicas (but watch out for latency).
- Each read replica will have its own DNS end point.
- You can have a read replica in another region.
- Read replicas can be promoted to be their own DB. This breaks the replication.

### ElastiCache
ElastiCache is an in-memory cached service, it supports **Memcached** and **Redis** caching engines.
- **Memcached**: If you don't need Multi-AZ (not concerned about redundancy), then you should use Memcached. It's designed as a pure caching solution with no persistence, ElastiCache manages Memcached nodes as a pool that can grow and shrink, similar to an EC2 auto scaling group. Use cases:
    - Object caching is primary goal, e.g., to offload the DB.
    - You want to use simple caching model.
    - You plan on running large cache nodes, and require multithreaded performance with utilization of multiple cores.
    - You want to scale your cache horizontally (scale out) as you grow.
- **Redis**: supports Mater/Slave replication and Multi-AZ which can be used to achieve cross AZ redundancy. Redis has features of replication and persistence, ElastiCache manages Redis more as a relational DB. Redis clusters are managed as stateful entities that include failover, similar to how Amazon RDS manages DN failover. Use cases:
    - You need to use advanced data types, such as lists, hashed, and sets.
    - You are doing data sorting and ranking (such as leader boards).
    - The persistence of your key store is important.
    - You want to run in Multi-AZ. with failover.
    - Pub/Sub capabilities are needed.

## S3

S3 is a safe place to store your files. It's Object-based, you can upload files from 0 bytes to 5 TB, there is unlimited storage. Files are stored in Buckets (like folder). The largest object that can be uploaded in a single PUT is 5 GB. S3 Object-based is just as files, objects contains:
- Key
- Value
- Version ID (for data consistency)
- Metadata
- Subresources (Bucket Policies, Access Control Lists and CORS, Transfer Acceleration)

S3 is an universal namespace, so names must be unique globally. It's HTTP service, when you upload files successfully, you'll get 200.

Read after Write consistency for PUTs of new Objects. Eventual Consistency for overwrite PUTs and DELETEs (can take some time to propagate).

For pricing, S3 charges:
- Storage per GB.
- Requests (GET, PUT, Copy, etc.).
- Storage management pricing (inventory, analytics, and object tags).
- Data management pricing (data transferred out of S3).
- Transfer acceleration (use of CloudFront to optimize transfers).

S3 does not currently support object locking for concurrent updates. If 2 PUT requests are simultaneously made to the same key, the request with the latest timestamp wins. If this is an issue, you will need to build an object-locking mechanism into your application.

Object locking is different from the [S3 Object Lock](https://docs.aws.amazon.com/AmazonS3/latest/dev/object-lock.html) feature. With S3 Object Lock, you can store objects using a write-once-read-many (WORM) model and prevent an object from being deleted or overwritten for a fixed amount of time or indefinitely.

**S3 Select** enables applications to retrieve only a subset of data from an object by using simple SQL expressions. By using S3 Select to retrieve only the data needed by your application.

**S3 (Server) Access Logs** provides detailed records for the requests that are made to a bucket. Server access logs are useful for many applications. For example, access log information can be useful in security and access audits. It can also help you learn about your customer base and understand your S3 bill.

**S3 Analytics** can analyze storage access patterns to help you decide when to transition the right data to the right storage class.

**S3 inventory** is one of the tools S3 provides to help manage your storage. You can use it to audit and report on the replication and encryption status of your objects for business, compliance, and regulatory needs.

### S3 Products

<img src="./screenshots/s3-products.png" alt="s3-products" />

S3 product|Feature
--|--
S3|durable, immediately available, frequently accessed
S3 - IA|durable, immediately available, infrequently accessed
S3 - One Zone IA|same as IA however data is stored in a single AZ
S3 - RRS|store reproducible data like thumbnails
Glacier|store archived data, where you can wait 3-5 hours before accessing

- S3: 99.99% availability, 99.999999999% durability, stored redundantly across multiple devices in multiple facilities.
- S3 - IA (Infrequently Accessed): For data that is accessed less frequently, but requires rapid access when needed. Lower fee than S3, but you are charged a retrieval fee.
- S3 - One Zone IA: Same as IA however data is stored in a single AZ only, still 11 9's durability, but only 99.5% availability. Cost is 20 % less than regular S3 - IA.
- Reduced Redundancy Storage (RRS): Designed to provide 99.99 durability and 99.99% availability of objects over a given year. Used for data that can be recreated if loss, e.g. thumbnails.
- Glacier: Very cheap, but used for archival only. Optimized for data that is infrequently accessed and it takes 3-5 hours to restore from Glacier (not real-time access).
- S3 - Intelligent Tiering: Used for unknown or unpredictable access pattern. Automatically moves your data to most cost-effective tier based on how frequently you access each object. 11 9's durability and 99.9% availability over a given year. No fees for accessing your data but a small monthly fee for monitoring/automation.

### Security
You can set up access control to your buckets using:
1. Bucket Polices (bucket level)
1. Access Control Lists (object level)

All newly created buckets are **private** by default. S3 buckets can be configured to create access logs. These logs can written to another bucket. This can be sent to another bucket and even another bucket in another account.

There are 3 types of S3 encryption:
- In transit: SSL/TLS.
- At rest (server side): If the file is to be encrypted at upload time, the `x-amz-server-side-encryption` header will be included. 2 options are available: `x-amz-server-side-encryption: AES256` (SSE-S3) or `x-amz-server-side-encryption: ams:kms` (SSE-KMS). When this parameter is included in the header of the PUT request, it tells S3 to encrypt the object at the time of upload. You can use Bucket Policy to deny any S3 PUT request which doesn't include `x-amz-server-side-encryption` header.
    - S3 Managed Keys (**SSE-S3**): Amazon manages keys for you automatically.
    - AWS Key Management Service, Managed Keys (**SSE-KMS**): you and Amazon manage the keys together.
    - Server Side Encryption with Customer Provided Keys (**SSE-C**): you give Amazon your own keys that you manage. S3 rejects any requests made over HTTP when using SSE-C (you ***MUST*** use HTTPS).
- Client side encryption: Encrypt data before sending it to Amazon S3.
    - Use a customer master key (CMK) stored in AWS Key Management Service (AWS KMS).
    - Use a master key that you store within your application.

### Encryption Lab
1. Services -> S3 -> create a bucket as default.
1. Click bucket you created -> **Permissions** tab -> click **Bucket Policy** -> **Policy generator**:
    - Choose **S3 Bucket Policy**.
    - Choose **Deny** effect.
    - Type `*` for **Principal**.
    - Check **PutObject** for **Actions**.
    - Go back to **Bucket Policy** in **Permissions** tab to get your S3 ARN.
    - Click **Add Conditions**. Choose **StringNotEquals** for condition, **s3:x-amz-server-side-encryption** for Key and type `aws:kms` for Value. Then click **Add Condition**.
    - Click **Add Statement**.
    - Click **Generate Policy**.
1. Copy the policy and paste to Bucket Policy.
    ```json
    {
        "Id": "Policy1598845749237",
        "Version": "2012-10-17",
        "Statement": [
            {
            "Sid": "Stmt1598845717887",
            "Action": [
                "s3:PutObject"
            ],
            "Effect": "Deny",
            "Resource": "arn:aws:s3:::my-bucket",
            "Condition": {
                "StringNotEquals": {
                "s3:x-amz-server-side-encryption": "aws:kms"
                }
            },
            "Principal": "*"
            }
        ]
    }
    ```
1. Click **Save**, you might get error: **Action does not apply to any resource(s) in statement**. That's because you need to specify the resources, so update Resource property in JSON to `Resource": "arn:aws:s3:::my-bucket/*` then click **Save** again.
1. Upload file. In **Set permissions** step and **Encryption** section, if you don't choose **AWS KMS master-key**, upload will fail.

### CORS Configuration Lab
1. Create a public bucket (uncheck **Block all public access**).
1. Click bucket you created -> switch to **Property** tab -> click **Static website hosting** and choose **Use this bucket to host a website**.
    - Index document: `index.html`
    - Error document: `error.html` (and hit save)
1. Upload [index.html](cors/index.html), [error.html](cors/error.html), and [loadpage.html](cors/loadpage.html) to this bucket. For **Manage public permissions**, choose **Grant public read access to this object(s)**. Then hit next to upload.
1. Go to **Properties** tab and open **Static website hosting**, click URL of **Endpoint**, you can see the result. Now the document are in the same bucket, you don't need to set up CORS to get `loadpage.html`.
1. Delete `loadpage.html` in your bucket.
1. Create another public bucket with same **Properties** setting with first bucket (same as step 2).
1. Upload [loadpage.html](cors/loadpage.html) to this bucket and choose **Grant public read access to this object(s)**.
1. Go to **Properties** tab and open **Static website hosting**, click URL of **Endpoint**, you will get 404. Don't worry, please add `/loadpage.html` after original URL like http://testcors.s3-website-us-east-1.amazonaws.com/loadpage.html, then you can see the `loadpage.html`.
1. Copy above URL and substitute it for `loadpage.html` in [index.html](cors/index.html) and save.
    ```html
    <!--CORS Code - Copyright ACloud.guru-->
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <html><body><h1>Welcome to the Index Page!</h1>

    <div id="get-html-from-other-s3"></div>

    <script>
    $("#get-html-from-other-s3").load("http://testcors.s3-website-us-east-1.amazonaws.com/loadpage.html")
    </script>
    </body></html>
    ```
1. Delete `index.html` in first bucket then upload your updated `index.html` with **Grant public read access to this object(s)** setting.
1. Now visit the first bucket Endpoint again, you will not get the second bucket. Because we don't have CORS setting.
1. Copy the Endpoint URL of first bucket.
1. Go to second bucket and choose **Permissions** tab, click CORS configuration. Paste [CORS configuration](cors/cors-configuration.txt) to it, remember to replace `*` with your first bucket Endpoint URL. Then hit save.
    ```
    <CORSConfiguration>
        <CORSRule>
            <AllowedOrigin>http://test.s3-website-us-east-1.amazonaws.com/</AllowedOrigin>
            <AllowedMethod>GET</AllowedMethod>
            <MaxAgeSeconds>3000</MaxAgeSeconds>
            <AllowedHeader>Authorization</AllowedHeader>
        </CORSRule>
    </CORSConfiguration>
    ``` 
1. Refresh first bucket Endpoint URL again, you can see the content in `loadpage.html`.

### CloudFront
It's a CDN (content delivery network) service in AWS. Objects are cached for the life of the TTL (time to live), you can clear cached objects, but you will be charged. CloudFront has 3 important concepts:
- **Edge location**: This is the location where content will be cached. This is separate to an AWS Region/AZ. Edge locations are not just READ only, you can WRITE to them too.
- **Origin**: This is the origin of all the files that the CDN will distribute. Origins can be an S3 Bucket, EC2 instance, ELB or Route 53.
- **Distribution**: This is the name given the CDN, which consists of a collection of Edge locations.
    1. Web distribution: Typically used for websites.
    1. RTMP: Used for media streaming.

**CloudFront Signed URLs** and **Signed Cookies** are different ways to ensure that users attempting access to files in an S3 bucket can be authorised. One method generates URLs and the other generates special cookies but they both require the creation of an application and policy to generate and control these items.

**CloudFront Origin Access Identity** is a virtual user identity that is used to give the CloudFront distribution permission to fetch a private object from an S3 bucket.

For S3 performance, there are 2 traditional ways to improve, but notice that **Mixed Workloads** is deprecated (AWS S3 handles 3500 PUT requests per second or 5500 GET requests per second since July 2018).
1. **GET - Instensive Workloads**: CloudFront will cache your most frequently access objects and will reduce latency for your GET request to improve S3 performance.
1. **Mixed Workloads** (deprecated): S3 uses the **key name** (object name) to determine which partition and object will be stored in. So you should avoid sequential key names for your S3 objects. Instead, add a random prefix like a hex hash to the key name to prevent multiple objects from being stored on the same partition.

### CloudFront Lab
1. Create a public bucket, choose Sydney Region.
1. Upload an image file (it's better more than 1 MB) with **Grant public read access to this object(s)** setting.
1. Services -> CloudFront -> Create Distribution -> select **Web** and hit **Get Started**.
    - Origin Domain Name: choose the S3 bucket you created.
    - Restrict Bucket Access: Yes.
    - Origin Access Identity: Create a New Identity.
    - Grant Read Permissions on Bucket: Yes, Update Bucket Policy.
    - Viewer Protocol Policy: Redirect HTTP to HTTPS.
    - Leave everything else as default and create distribution.
1. Go back to S3 and click on your image file. Copy Object URL like https://test-origin.s3-ap-southeast-2.amazonaws.com/businessman.png.
1. Click the CloudFront Distribution you created, you can see the **Domain Name** like `d111111111abc.cloudfront.net`. Replace above step URL with your DNS like https://d111111111abc.cloudfront.net/businessman.png then you will see the result.
1. Open another type of browser to visit above URL again, you will find the loading speed is really fast.
1. Click on CloudFront Distribution you created and **Disable** it. If status is **Deployed** then you can **Delete** it.

## Lambda

It's a compute service where you can upload your code and create a lambda function. Lambda takes care of provisioning and managing the services that you use to run the code.

As an event-driven compute service where Lambda runs your code in response to events. These events could be changes to data in an Amazon S3 bucket or Amazon DynamoDB table. Supported event sources include: CloudWatch, DynamoDB, S3, Kinesis, CodeCommit, IoT buttons, CloudFront, Cognito, SNS, SQS, SES etc. But RDS cannot trigger a function directly.

As a compute service to run your code in response to HTTP requests using Amazon API Gateway or API calls made using AWS SDKs. Lambda has following features:

- Lambda scales out (not up) automatically.
- Lambda functions are independent, 1 event is 1 function.
- Lambda is serverless.
- Lambda functions can trigger other lambda functions, 1 event can trigger N functions if functions trigger other functions.
- Lambda stores code in S3 and encrypts it at rest and performs additional integrity checks while the code is in use.
- You cannot set up CPU directly. You can choose memory for your function then Lambda allocates corresponding CPU power for you.
- Each Lambda function receives 500 MB of non-persistent disk space in its own `/tmp` directory.
- Lambda functions should be stateless, state can be maintained externally in DynamoDB or S3.
- But architectures can get extremely complicated and be hard to debug, AWS X-Ray allows you to debug what's happening.

Notice that the Lambda concurrent executions limit exists. The limit is 1,000 concurrent executions per second. If you hit the limit, you will see invocations being rejected and get 429 HTTP code. You can get the limit raised by AWS support, or use **reserved concurrency** to guarantee that a set number of executions are always available to a critical function.

All calls made to AWS Lambda must complete execution within 900 seconds. Default timeout is 3 seconds. Timeout can be set the timeout to any value between 1 and 900 seconds.

It's possible to enable Lambda to access resources which are inside a private VPC. You need to provide VPC config (private subnet ID and security group ID) to the function. Lambda uses the VPC information to set up ENIs using an IP from the private subnet CIDR range. Notice that lambda function should have a role with **AWSLambdaVPCAccessExecutionRole** policy then you can set up VPC.

Lambda functions have the following restrictions:
- Inbound network connections are blocked by AWS Lambda.
- Outbound connections only TCP/IP and UDP/IP sockets are supported.
- ptrace (debugging) system calls are blocked.
- TCP port 25 traffic is also blocked as an anti-spam measure.

For failure handling:
- For S3 bucket notifications and custom events, Lambda will attempt execution of the function 3 times in the event of an error condition in the code or if a service or resource limit is exceeded.
- For ordered event sources like DynamoDB Streams or Kinesis streams which Lambda polls, it will continue attempting execution in the event of a developer code error until the data expires.
- Kinesis and DynamoDB Streams retain data for a minimum of 24 hours.
- *Dead-letter queues* can be configured to save discarded events for further processing, once the retry policy for asynchronous invocations is exceeded.

You can share code across functions by packaging any code (frameworks, SDKs, libraries, and more) as a [Lambda Layer](https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html) and manage and share them easily across multiple functions.

A *deployment package* is a ZIP archive that contains your function code and dependencies. You can upload the package directly to Lambda, or you can use an S3 bucket, and then upload it to Lambda. If the deployment package is larger than 50 MB, you must use S3.

When a Lambda function is invoked, Lambda launches an *Execution Context* based on the provided configuration settings. After a Lambda function is executed, Lambda maintains the *Execution Context* for some time in anticipation subsequent function invocation. So the subsequent invocations perform better performance as there is no need to *cold-start* or initialize those external dependencies. Notice that Lambda manages *Execution Context* creations and deletion, there is no AWS Lambda API to manage *Execution Context*.

For CI/CD, you can deploy and manage your serverless applications using the AWS Serverless Application Model (AWS SAM). SAM aligns with the syntax used by CloudFormation and is supported natively within CloudFormation as a set of resource types. You can also automate your serverless application's release process using CodePipeline and CodeDeploy.

### API Geteway
API Gateway is an AWS service for creating, publishing, maintaining, monitoring, and securing REST, HTTP, and WebSocket APIs at any scale. It has following features:

- API Gateway supports HTTP, REST and WebSocket APIs.
- API Gateway expose HTTPS endpoints only for all the APIs created. It does not support HTTP endpoints.
- Serverlessly connect to services like Lambda and DynamoDB.
- Send each API endpoint to a different target.
- Run efficiently with low cost and scale effortlessly.
- Track and control usage by API key.
- Throttle requests to prevent attacks.
- Connect to CloudWatch to log all requests for monitoring.
- API Geteway allows multiple API versions and multiple stages (development, production etc.) for each version simultaneously so that existing applications can continue to call previous versions after new API versions are published.
- API Gateway has caching capabilities to increase performance.
- You can import existing API using swagger 2.0 definition files.
- API Gateway supports SOAP applications but only provide pass-through. API Gateway doesn't transform or convert the responses.

For **API Caching**, when you enable caching for a stage, API Gateway caches responses from your endpoint for a specific time-to-live (TTL) period, in seconds. API Gateway then responds to the request by looking up the endpoint response from the cache instead of making a request to your endpoint.

For **API throttling**, by default, API Gateway limits the steady-state request rate to 10,000 requests per second (rps). The maximum concurrent requests is 5,000 requests across all APIs within an account. If you go over these limits, you will receive a 429 Too Many Request error response.

To deploy an API, you create an API deployment and associate it with a stage. A stage is a logical reference to a lifecycle state of your API (for example, dev, prod, beta, v2). API stages are identified by the API ID and stage name. Notice that every time you update an API, you ***MUST*** redeploy the API to an existing stage or to a new stage.

You can define *usage plan* to specify who can access one or more deployed API stages and methods, and also how much and how fast they can access them. *API keys* are alphanumeric string values that you distribute to application developer customers to grant access to your API. You can use API keys together with usage plans or Lambda authorizers to control access to your APIs.

Sometimes you suspect that your function is taking too long to execute, you should check X-Ray and API Gateway integration latency metric in CloudWatch which measures the time between when API Gateway relays a request to the backend and when it receives a response from the backend.

### API Gateway Lambda Authorizer
A Lambda authorizer is an API Gateway feature that uses a Lambda function to control access to your API. A Lambda authorizer is useful if you want to implement a custom authorization (3rd party authorization) scheme that uses a bearer token authentication strategy such as OAuth or SAML, or that uses request parameters to determine the caller's identity.

API Gateway checks whether a Lambda authorizer is configured for the method. If it is, API Gateway calls the Lambda function. The Lambda function authenticates the caller by means such as the following:

- Calling out to an OAuth provider to get an OAuth access token.
- Calling out to a SAML provider to get a SAML assertion.
- Generating an IAM policy based on the request parameter values.
- Retrieving credentials from a database.

<img src="screenshots/custom-auth-workflow.png" alt="custom-auth-workflow"/>

### Serverless Website Lab
1. Create a public S3 bucket (named e.g. `mytest.com`).
1. Go to **Properties** of this bucket and enable **Static website hosting**.
    - Index document: `index.html`
    - Error document: `error.html` (and hit save)
1. Go to Lambda -> Functions -> create function.
    - Nmae: `MyServerlessWeb`.
    - Runtime: **Python 3.X**.
    - Execution Role: **Create a new role from AWS policy templates**.
    - Role name: `MyLambdaRole`.
    - Policy templates: **Simple Microservice permissions**.
1. The page will redirect to function you created after creating complete. In **Function code** section, copy the content in [serverless.py]() and replace all code in **lambda_function.py** file with it.
    <img src="screenshots/function-code.png" alt="function-code"/>
1. In **Designer** section, click on **+ Add trigger**.
    - Choose **API Gateway**.
    - API: Create an API.
    - API type: HTTP API.
    - Security: Open.
    - API name: `MyServerlessWeb-API`.
    - Deployment stage: `prod`.
1. Click on API Gateway you created, you will be redirected to API Gateway page. Look at left panel, click **Develop** -> **Routes**.
    <img src="screenshots/api-gateway-route.png" alt="api-gateway-route"/>
1. Click on **ANY** then edit it. Change method to **GET**.
1. Click on GET again, then click **configure**, you can see this API is integrated to your Lambda function.
1. Click on **Stages** (under **Deploy**) in left panel. Choose **prod**, notice that this stage is automatically deployed by AWS for you. **Invoke URL** is the root of router. So if you want to send a GET request to your lambda function, you need to add `/MyServerlessWeb` after Invoke URL. e.g., `https://d123456789.execute-api.us-west-2.amazonaws.com/prod/MyServerlessWeb`.
1. Open [index.html](serverless-web/index.html), replace `YOUR_API_GATEWAY_LINK` with your API Gateway link.
1. Upload updated `index.html` and [error.html](serverless-web/error.html) to your S3 bucket and choose **Grant public read access to this object(s)**.
1. After uploading, choose both file and **Make public**.
1. Switch to **Properties** tab and click on **Static website hosting**, you can find your bucket's Endpoint URL. Visit this URL then you finish.
1. If you want to use custom domain name by Route 53, please refer to [here](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html).

### Lambda Versioning
You can use versions to manage the deployment of your Lambda functions. It has following facts:

- Each lambda function version has a unique Amazon Resource Name (ARN).
- After you publish a version, it's immutable.
- AWS maintains your latest function code in the `$LATEST` version.
- You can use alias to point latest version.
- If the function ARN with the version suffix, we call it Qualified ARN like `arn:aws:lambda:us-west-2:123456789012:function:test:$LATEST`. Otherwise, it's Unqualified ARN like `arn:aws:lambda:us-west-2:123456789012:function:test`.
- You can split traffic using aliases to different versions (Blue/Green Deployment), but you cannot split traffic with `$LATEST`, instead create an alias to latest.

### Lambda Versioning Lab
1. Create a function named `myFunc`.
1. Go to **Function code** section, replace `Hello from Lambda!` with `Hello` and hit save:
    ```js
    exports.handler = async (event) => {
        // TODO implement
        const response = {
            statusCode: 200,
            body: JSON.stringify('Hello'),
        };
        return response;
    };
    ```
1. Click on **Qualifiers** (at the top) -> Versions -> $LATEST. Then you can notice that the ARN will be changed to `arn:aws:lambda:us-west-2:123456789012:function:myFunc:$LATEST`.
1. Click on **Actions** -> Publish new version -> type `v1` for **Version description** -> hit publish. Then ARN is changed to `arn:aws:lambda:us-west-2:123456789012:function:myFunc:1`. 
1. After publishing, you will find you are not allowed to update the code of `v1` version anymore. Editing is only available for the **$LATEST** version.
1. Go to version $LATEST and replace `Hello` with `Hello World!`. Click on save and publish version as `v2`.
1. Go to version $LATEST and replace `Hello World!` with `Hello Lambda!`. Click on save and publish version as `v3`.
1. Click **Actions** -> Create alias.
    - Name: version1.
    - Description: version1.
    - Version: 1.
1. After creating, you can notice that the ARN is changed to `arn:aws:lambda:us-west-2:123456789012:function:myFunc:version1`.
1. Create another alias named `latest` to point to version **$LATEST**. The ARN of latest version will be `arn:aws:lambda:us-west-2:123456789012:function:myFunc:latest`.
1. Finally, you can deploy the function for Blue/Green Testing. e.g., you can deploy the function with 75% `v2` and 25% `v3` as following setting. Notice that you cannot mix any version with **$LATEST**.
    <img src="screenshots/split.png" alt="split"/>

### Step Functions
Step Function allows you to visualize and test your serverless applications. It automatically triggers and tracks each step, and retries when there are errors. Step functions also logs the state of each step. If you want to delete the lambda created by step functions, you can go to CloudFormation to delete.

### Step Functions Lab
1. Services -> Step Function -> State machines -> Create state machine.
1. Choose **Run a sample project** -> choose **Job Poller** -> go next -> Deploy resources (it will take about 10 min, please don't close that page).
1. When complete, the New execution page is displayed.
    1. Enter an execution name: `MyExecution`.
    1. Check on **Open in a new browser tab**.
    1. Then click **Start execution**.
1. If you want to check the job, please go to Services -> Batch.
1. After it finished, go to CloudFormation and delete stacks.

### AWS X-Ray
It's a service that collects data about requests that your application serves, and provides tools you can use to view, filter, and gain insights into that data. X-Ray SDK provides:
- Interceptors to add to your code to trace incoming HTTP requests.
- Client handlers to instrument AWS SDK clients that your application uses to call other AWS services.
- An HTTP client to use to instrument calls to other internal and external HTTP web services.

**X-Ray SDK** sends the data to the **X-Ray daemon** which buffers segments in a queue and uploads them to X-Ray in batches. In ECS, create a Docker image that runs the X-Ray daemon, upload it to a Docker image repo and then deploy it to your ECS cluster. You can use port mappings and network mode settings in your task definition file to allow your application to communicate with the daemon container.

*X-Ray sampling* is to control the amount of data that you record, and modify sampling behavior on the fly without modifying or redeploying your code. Sampling rules tell the X-Ray SDK how many requests to record for a set of criteria.

You can record additional information about requests, the environment, or your application with *annotations* and *metadata*.
- *Annotations* are key-value pairs with string, number, or Boolean values. Annotations are indexed for use with *filter expressions*. Use annotations to record data that you want to use to group traces in the console, or when calling the `GetTraceSummaries` API.
- *Metadata* are key-value pairs that can have values of any type, including objects and lists, but are ***NOT*** indexed for use with *filter expressions*. Use metadata to record additional data that you want stored in the trace but don't need to use with search.


## DynamoDB
DynanoDB is a fast NoSQL database service. All data is stored in SSD. There are following features:
- Consists of Tables, Items (row), and Attributes (column).
- Supports both document (JSON, HTML and XML) and key-value data models.
- DynamoDB does not support *joins* operation.
- 2 types of Primary key:
    - Partition key: It's unique attribut like ID.
    - Composite key (Partition key + Sort key): All items with the same partition key value are stored together, in sorted order by sort key value.
- 2 consistency models:
    - Eventually consistent (default): It offers low latency but may reply to read requests with stale data since all nodes of the database may not have the updated data.
    - Strongly consistent: It offers up-to-date data but at the cost of high latency.
- The maximum item size in DynamoDB is 400 KB, which includes both attribute name binary length (UTF-8 length) and attribute value lengths (again binary length).
- AWS places some [default quotas](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html#default-limits-throughput-capacity-modes) on the throughput you can provision, you can contact service support to request more quota.
- Access is controlled using IAM policies. `dynamodb:LeadingKeys` is to allow users to access only the items that the partition key value matches their user ID.

**DynamoDB Stream** is time-ordered sequence log of item level modifications in your DynamoDB Tables. The data is stored for 24 hours only. It can be used as an event source for lambda so you can create applications which take actions based on events in your DynamoDB table.

For API call, you can use `GetItem` operation to get an item with the given primary key. `BatchGetItem` operation returns one or more items from one or more tables. You identify requested items by primary key. A single operation can retrieve up to 16 MB of data, which can contain as many as 100 items.

If 2 editors are working on the same content, DynamoDB might happen overwriting issue. You can use a *condition-expression* in the `UpdateItem` command to prevent.

### Indices
There are 2 types of index are supported to help speed-up your queries:

Local Secondary Index (LSI)|Global Secondary Index (GSI)
--|--
Must be created at when you create table|Can create any time
Same Partition Key as your table|Can choose different Partition Key
Can choose different Sort Key|Can choose different Sort Key

If you perform heavy write activity on the table, but a GSI on that table has insufficient write capacity, then the write activity on the table will be throttled. To avoid potential throttling, the provisioned write capacity for a GSI should be equal or greater than the write capacity of the base table since new updates will write to both the base table and GSI. Notice that LSI uses the RCU and WCU of the main table, so you don't provision more RCU and WCU to the LSI.

### Query vs Scan
Query has following facts:
- Find items in a table using only the Primary Key attribute. You can also use an optional Sort Key to refine the results.
- By default, returns all data attribute, you can use the `ProjectionExpression` parameter to refine the results.
- Results are always sorted by Sort Key (ascending order), you can reverse the order by setting the `ScanIndexForward` to false.
- By default, Queries are Eventually Consistent.

Scan has following facts:
- It examines every item in the table. e.g. if you want to find out ID = 32 item, the only way is to use **filter**. That means you use Scan to dump the entire table then filter out the value to provide the desired result.
- By default, returns all data attribute, you can use the `ProjectionExpression` parameter to refine the results.|Same with Query.

To compare performance between Query and Scan:
- Query is more efficient than a Scan.
- You can improve performance by setting a smaller page size (large number of smaller operations will allow other requests to succeed without throttling).
- Avoid using Scan operations if you can.
- By default, a Scan operation processes data sequentially in returning 1 MB increments before moving on to retrieve the next 1 MB of data. You can configure DynamoDB to use parallel scans. A parallel scan with a large number of workers can easily consume all of the provisioned throughput for the table or index being scanned. It is best to avoid such scans if the table or index is also incurring heavy read or write activity from other applications.

### Read/Write Capacity Mode
DynamoDB has 2 read/write capacity modes for processing reads and writes on your tables: On-demand, Provisioned (default, free-tier eligible).
- Provisioned mode:
    - If you choose provisioned mode, you specify the number of reads and writes per second that you require for your application.
    - Notice that provisioned throughput is the maximum amount of capacity that an application can consume from a table or index. If your application exceeds your provisioned throughput capacity on a table or index, it is subject to request throttling.
    - When the number of requests is too high, you will see a `ProvisionedThroughputExceededException`. SDK will automatically retries using **Exponential Backoff** which means the time betweeen retries will be progressively longer. e.g. 50 ms, 100 ms, 200 ms, 400 ms... for improved flow control.

    Write Capacity Units (WCUs)|Read Capacity Units (RCUs)
    --|--
    1 WCU = 1 x 1 KB Write per second|1 RCU = 1 x 4KB Strongly Consistent Read per second<br>**OR**<br>1 RCU = 2 x 4KB Eventually Consistent Read per second

- On-demand mode:
    - You don't need to specify your requirements. DynamoDB instantly scales up and down based on the activity of your application.
    - It's suitable for spiky, short lived peaks.
    - You are charged by pay-per-use model.

### DynamoDB Accelerator (DAX)
DAX is a fully managed, clustered in-memory cache for DynamoDB.
- Improves response times for **Eventually Consistent** reads only.
- You point your API calls the DAX cluster, instead of your table.
- If the item you are querying is on the cache, DAX will return it. Otherwise it will perform an Eventually Consistent `GetItem` operation to your DynamoDB table.
- Not suitable for write-intensive applications or applications that require Strongly Consistent reads.

### ElastiCache
ElastiCache is in-memory cache in the cloud. There are 2 types:
- **Memcached**: Widely adopted memory object caching system, multi-thread but no multi-AZ capability.
- **Redis**: It's a in-memory key-value store, supports more complex data structures (e.g. sorted sets and lists). It supports master/slave replication and multi-AZ for cross AZ redundancy.

There are 2 caching strategies:
- **Lazy loading**: Loads the data into the cache only when necessary.
    <img src="screenshots/lazy-loading.png" alt="lazy-loading"/>
    - *Cache hit*: If requested data is in the cache, ElastiCache returns it.
    - *Cache miss*: If the data is not in the cache or has expired, ElastiCache returns a `null`. Then your application requests and receives the data from the database. Finally your application updates the cache with the new data.
    - To avoid to keep stale data, you can specify the TTL to expire the data. This method cannot eliminate stale data, but it keeps data from getting too stale.
- **Write-through**: adds data or updates data in the cache whenever data is written to the database. So every write involves 2 trips: a write to the cache and a write to the database. Notice that if a node fails and a new one is spun up, data is missing until added or updated in the database.

Caching Strategies|Advantages|Disadvantages
--|--|--
Lazy Loading|1. Only requested data is cached.<br>2. Node failures aren't fatal for your application.|1. There is a cache miss penalty.<br>2. Stale data.
Write-Through|1. Data in the cache is never stale.<br>2. Cache hit every time.|1. Write penalty.<br>2. Missing data.<br>3. Most data is never read, which is a waste of resources.

### TTL
TTL (Time-to-live) attribute defines an expiry time for your data. If time is up the data will be marked for deletion automatically then be deleted in 48 hours.

TTL expressed as epoch time like `1544023618`. When the current time is greater than the TTL, the item will be expired and marked for deletion.

### TTL Lab
1. Services -> IAM -> Users -> Add user
    - Name: `developer`.
    - Access type: check **Programmatic access**.
    - Set permissions: **Attach existing policies directly**.
    - Policies: **AdministratorAccess**.
1. After creating, please copy **Access key ID** and **Secret access key** values, you will need to use them in next step.
1. Open your terminal and sign in as IAM User who is able to access DynamoDB. You can delete `~/.aws/config` and `~/.aws/credentials` to clean the configuration first. Type your **Access key ID** and **Secret access key**, then type the region you like (e.g. `us-west-2`).
    ```
    aws configure
    ```
1. Check your IAM user now (should be `developer`).
    ```
    aws iam get-user
    ```
1. Confirm you have permission to access DynamoDB.
    ```
    aws iam list-attached-user-policies --user-name developer
    ```
1. Create `SessionData` table.
    ```
    aws dynamodb create-table --table-name SessionData --attribute-definitions \
        AttributeName=UserID,AttributeType=N --key-schema \
        AttributeName=UserID,KeyType=HASH \
        --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5
    ```
1. Dump data into `SessionData` table.
    ```
    aws dynamodb batch-write-item --request-items file://dynamodb/items.json
    ```
1. Go back to DynamoDB console and select `SessionData` table. You will find 3 items in there, please edit the values in column **ExpirationTime** to set up when to expire (you can get epoch time [here](https://www.epochconverter.com/)).  
1. Click **Actions** -> Manage TTL:
    - TTL attribute: `ExpirationTime`.
    - You can run preview to test, then click continue.
1. In **Overview** tab of this table, you can see **Time to live attribute** is set to **ExpirationTime**. Then you will find the data is marked for deletion after the epoch time you set up.

## KMS

AWS Key Management Service (KMS) is to create and control the encryption keys used to encrypt your data. Customer Master Key (CMK) is used for generating/encrypting/decrypting the **Data Key** which is used to encrypt/decrypt data > 4 KB. CMK also can encrypt and decrypt up to 4 KB of data, but CMK can never be exported (CMK stays inside KMS).

Notice that AWS KMS does not store, manage, or track the data keys. You must use them outside of AWS KMS.

### CMK Lab

1. Services -> IAM -> Groups -> Create New Group:
    - Group Name: `myKMSGroup`.
    - Attach Policy: check `AdministratorAccess`.
1. IAM -> Users -> Add user:
    - User name: `myKeyManager`
    - Access type: check both **Programmatic access** and **AWS Management Console access**.
    - Console password: Choose **Custom password** then input your password.
    - Require password reset: uncheck **User must create a new password at next sign-in**. Then go next.
    - Add user to group: select `myKMSGroup` group to add.
1. Create another user named `myKeyUser` with same setting with `myKeyManager`.
1. Services -> Key Management Service -> Create key in region **us-west-2** (notice that the region you choose, you can only encrypt/decrypt the data stored in this region):
    - Key type: Symmetric.
    - Create alias: `myCMK`.
    - Define key administrative permissions: check **myKeyManager**.
    - Define key usage permissions: check **myKeyUser**.
1. Services -> EC2 -> Launch instance in region **us-west-2**:
    - Configure Instance DetailsAuto: set enable for **assign Public IP**.
    - Configure Security Group: SSH and source is open to `0.0.0.0/0`.
    - Create a new key pair named `kmskp`.
1. Open your terminal and connect to your EC2 instance. You can your IP address in your EC2 instance property **Public IPv4 address**.
    ```
    cd ~/Downloads
    chmod 400 kmskp.pem
    ssh ec2-user@51.189.123.12 -i kmskp.pem
    ```
1. Create a `secret` file.
    ```
    echo "Hello KMS! " > secret.txt
    ```
1. Services -> IAM -> Users -> select **myKeyUser** -> **Security Credentials** tab.
    1. You can find you have a existing access key in there, click **Make inactive** then click X mark to delete it.
    1. Click **Create access key**, copy the values of **Access key ID** and **Secret access key** and store them somewhere.
1. Go back to your terminal to configure settings. Paste your access Key and secret key.
    ```
    > aws configure
    AWS Access Key ID [None]: AKIA36EYK7GUJK53RYZT
    AWS Secret Access Key [None]: EEG/FX+w8+Pnequ+xtyZ9TQtwAW1Mbv/cbigqA15
    Default region name [None]: us-west-2
    Default output format [None]: text
    ```
1. Services -> KMS -> Customer managed keys -> copy your key ID of alias **myCMK**.
1. Go back to your terminal and encrypt your `secret.txt` file. Use your key ID for parameter `--key-id`.
    ```
    aws kms encrypt --key-id 6d84ef61-491b-4728-9557-8f1311e73cd0 --plaintext fileb://secret.txt --output text --query CiphertextBlob | base64 --decode > encryptedsecret.txt
    ```
1. Decrypt `encryptedsecret.txt`. Now you can check whether the data in `decryptedsecret.txt` is same with `secret.txt`.
    ```
    aws kms decrypt --ciphertext-blob fileb://encryptedsecret.txt --output text --query Plaintext | base64 --decode > decryptedsecret.txt
    ```
1. Re-encrypt the `secret.txt`. This command is useful when you want to use another key ID to encrypt the data.
    ```
    aws kms re-encrypt --destination-key-id 6d84ef61-491b-4728-9557-8f1311e73cd0 --ciphertext-blob fileb://encryptedsecret.txt | base64 > newencryption.txt 
    ```
1. Enable key rotation automatically.
    ```
    aws kms enable-key-rotation --key-id 6d84ef61-491b-4728-9557-8f1311e73cd0
    ```
1. Check the status of key rotation, it should return `True`.
    ```
    aws kms get-key-rotation-status --key-id 6d84ef61-491b-4728-9557-8f1311e73cd0
    ```
1. Generate the data key.
    ```
    aws kms generate-data-key --key-id 6d84ef61-491b-4728-9557-8f1311e73cd0 --key-spec AES_256
    ```

### Envelope Encryption
A process for encrypting your data. CMK is used to encrypt the data key (also known as envelope key). And data key encrypts the data. Envelope Encryption is used for encrypting anything over 4 KB.

Why not encrypt the data using the CMK directly?
- Network: When you encrypt data directly with KMS, it must be transferred over the network (CMK is managed by KMS, it cannot be exported).
- Performance: With envelope encryption, only the data key goes over the network instead of your data. So the data key is used locally in your application.

## Other Services

### SQS
Simple Queue Service (SQS) is a distributed message queuing system. It's **pull-based** instead of push-based. Messages stored in queue can contain up to 256 KB of text in any format. Messages can be kept in the queue from 1 minute to 14 days (default is 4 days). SQS guarantee that messages will be processed at least once. There are 2 types of queue: 
- **Standard Queues** (default): Best-effort ordering, message delivered at least once (it might be more than one).
- **FIFO Queues**: Ordering strictly preserved, and message delivered once without duplicates. FIFO queues are limited to 300 transactions per second (TPS).

The messages in FIFO queue have parameters:
- Message deduplication ID: The token used for deduplication of sent messages. If a message with a particular message deduplication ID is sent successfully, any messages sent with the same message deduplication ID are accepted successfully but aren't delivered during the 5-minute deduplication interval.
- Message group ID: The tag that specifies that a message belongs to a specific message group. Messages that belong to the same message group are always processed one by one, in a strict order relative to the message group (however, messages that belong to different message groups might be processed out of order).


There are no message limits for storing in SQS, but *in-flight messages* do have limits. Make sure to delete messages after you have processed them. There can be a maximum of approximately 120,000 in-flight messages (received from a queue by a consumer, but not yet deleted from the queue).

**SQS visibility timeout** is the amount of time that message is invisible in the SQS queue after a reader picks up that message. The default is 30 seconds. So if your task takes more than 30 seconds, you should increase it to prevent that message is visible and be processed by another task (then it duplicates). The maximum value is 12 hour. You can use `ChangeMessageVisibility` API to change the visibility timeout.

Amazon SQS provides **short polling** and **long polling** to receive messages from a queue. By default, queues use short polling.
- Short polling: returns immediately even if no messages are in the queue.
- Long polling: polls the queue periodically and only returns a response when a message is in the queue or the timeout is reached. The maximum long polling wait time is 20 seconds.

**SQS delayed queue** can postpone delivery of new messages to a queue from 0 second (default) to 900 second. Messages sent to delayed queue remain invisible to consumers. Use cases:
- Large distributed applications which may need to introduce a delay in processing.
- You need to apply a delay to an entire queue of messages.

For large SQS messages (256 KB up to 2 GB), you have to use S3 to store them. And use **Amazon SQS Extended Client Library for Java** and **AWS SDK for Java** to manage them. You cannot use AWS CLI, SQS API, AWS console to control.

### SNS
Simple Notification Service (SNS) is a notification service which allows you to send push notifications. It's **push-based**. SNS supports variety format like SMS, email, SQS and HTTP endpoint.

SNS notifications can trigger Lambda functions. SNS also allows you to group multiple recipients using topics. For example, when you publish once to a topic, SNS can delivers formatted copies of your message to iOS, Android and SMS.

### SES
Simple Email Service (SES) is a email service. SES can trigger Lambda functions or SNS. It can be used for incoming and outgoing mail. It's not subscription based, you only need to know the email address.

### Kinesis
Kinesis is a service to load and analyze streaming data.
- **Kinesis Data Streams**: It's a real-time data streaming service. The data from producers (like EC2, iPhone, computer) is stored in shard which has a sequence of data records and retends for 24 hour to 7 days. Then you can use consumers (like EC2, Lambda) to consume data.
- **Kinesis Data Firehose**: It's full managed streaming service. It delivers data to S3.
- **Kinesis Data Analytics**: It's a service to analyze streaming data in real time with Apache Flink.

For Kinesis Data Streams, by default, the 2MB/second/shard output is shared between all of the applications consuming data from the stream. you should use enhanced *fan-out* if you have multiple consumers retrieving data from a stream in parallel. With enhanced fan-out developers can register stream consumers to use enhanced fan-out and receive their own 2MB/second pipe of read throughput per shard, and this throughput automatically scales with the number of shards in a stream.

For consumer, you have to use Kinesis Client Library (KCL) to create a **record processor** for each shard that is consumed by your instance. If you inscrease the number of shards, the KCL will add more record processors on your consumer. You should ensure **the number of instances ***DOES NOT*** exceed the number of shards**, because you never need multiple instances to handle the processing load of one shard. However, one worker with multiple record processors can process multiple shards.

### Kinesis Lab
1. Service -> CloudFormation -> Create stack:
    - Prepare template: Template is ready.
    - Template source: Amazon S3 URL.
    - Amazon S3 URL: `https://s3.amazonaws.com/kinesis-demo-bucket/amazon-kinesis-data-visualization-sample/kinesis-data-vis-sample-app.template`.
    - Stack name: `MyKinesisStack`.
    - In review page, check **I acknowledge that AWS CloudFormation might create IAM resources**.
1. After the status of your stack is changed to **CREATE_COMPLETE**, click stack and switch to **Outputs** tab. Then click URL, you can see the real time streaming data.
1. Services -> Kinesis -> Data streams -> click your data stream created by CloudFormation.
1. Switch to **Configuration** tab, you can see the number of open shards is 2 and data retention is 24 hours.
1. Go to DynamoDB console, you can find 2 tables is created by CloudFormation as well.
1. If you are comfortable, then you can go to CloudFormation and delete `MyKinesisStack` stack to clean up all resources.

### Elastic Beanstalk
For basic concept, please refer to [here](../../level-1/aws-certified-cloud-practitioner/README.md#elastic-beanstalk). Elastic Beanstalk supports several deployment policies:
1. **All at once**: Deploys the new version to all instances simultaneously, so there's down time. If the update fails, you need to roll back by re-deploying the original version.
1. **Rolling**: Deploys the new version in batches, so each batch of instances is taken out of service. If the update fails, you need perform a further rolling update.
1. **Rolling with additional batch policy**: Launches an additional batch of instances for update. It maintains full capacity during the deployment. If the update fails, you need perform a further rolling update.
1. **Immutable**: Deploys the new version to a fresh group of instances in their own new autoscaling group. When the new instances pass health checks, they are moved to your existing autoscaling group. Finally, the old instances are terminated. It maintains full capacity during the deployment. If the update fails, you just need to terminate the new autoscaling group. This policy is preferred for mission critical production systems.
1. **Blue/Green**: It's same with immutable. Once the new environment is up and passed all tests, traffic is shifted to this new deployment.

You can customize the Elastic Beanstalk environment by adding configuration files written in YAML or JSON format. Those `.config` files are saved to the `.ebextensions` folder which must be included in the top-level directory of your application source code bundle.

You can launch the RDS instance from within the Elastic Beanstalk, this is a good option for Dev and Test environment. However this is not ideal for production environment, because your database is tied to the lifecycle of your application. So for the production, you should launch RDS outside of Elastic Beanstalk. You need to add security group and provide connection information to EC2.

If you want to migrate Elastic Beanstalk environment from AWS A account to AWS B account, please follow the steps. For more details, please refer to [here](https://aws.amazon.com/premiumsupport/knowledge-center/elastic-beanstalk-migration-accounts/).
1. Create a saved configuration in Account A.
1. Download the saved configuration to your local machine.
1. Change your account specific parameters in the downloaded configuration file, and then save the changes.
1. Upload the saved configuration from your local machine to an S3 bucket in Account B.
1. From Elastic Beanstalk console in Account B, create an application from **Saved Configurations**.

### Systems Manager Parameter Store
You can store confidential information (e.g. password, license code) in Systems Manager Parameter Store. You can store values as plain text (`String` or `StringList` type) or encrypted data (`SecureString` type). You can reference these values by using their names.

Many aspects of Parameter Store and Secrets Manager appear very similar, but Secrets Store charges you for storing each secret and also provides a secret rotation service whereas Parameter Store does not. 

## Developer Theory (CI/CD)

For continuous integration (CI), you can use CodeCommit to store the code base and control version. For continuous delivery (CD), you can use CodeBuild and CodeDeploy. For continuous deployment, you can use CodePipeline which manages the whole workflow.

When CodeBuild runs a build:
1. CodeBuild will create a temporary compute container of the class defined in the build project.
1. CodeBuild loads it with the specified runtime environment.
1. CodeBuild downloads the source code.
1. CodeBuild executes the commands configured in the project.
1. CodeBuild uploads the generated artifact to an S3 bucket.
1. CodeBuild destroys the compute container.

During the build, CodeBuild will stream the build output to the service console and Amazon CloudWatch. If you want to receive notifications for any event in CodeBuild, you can use SNS.

Downloading dependencies is a critical phase in the build process. If you want to speed this process up, you can cache dependencies on S3.

### CodeDeploy
CodeDeploy is a deployment service that automates application deployments to Amazon EC2 instances, on-premises instances, serverless Lambda functions, or Amazon ECS services. If you want to receive notifications for any event in CodeDeploy, you can use SNS. To deploy your code by using CodeDeploy:
1. Create an application and deployment group (where to deploy, EC2 or Lambda).
1. Upload a revision (which version of code to deploy).
1. Deploy it.

CodeDeploy provides 2 deployment type options:
- **In-place**: It's same with **all at once** in Elastic Beanstalk. The application on each instance in the deployment group is stopped and the new version is installed. Also known as a **rolling update**. You can use a load balancer so that each instance is deregistered during its deployment and then restored to service after the deployment is complete. If you want to roll back, you need to re-deploy. Notice that Lambda and Amazon ECS deployments cannot use an in-place deployment.
- **Blue/Green** (recommended): The new version is installed on the replacement instances. After deployment is complete, replacement instances are registered with an ELB, instances in the original environment are deregistered. If you want to roll back, you can use ELB to switch to old version (if they are not deleted). But the shortcoming is you pay for 2 environments until terminating the old instances.

CodeDeploy rolls back deployments by redeploying a previously deployed revision of an application as a new deployment. When automatic rollback is initiated, or when you manually initiate a redeployment or manual rollback, CodeDeploy first tries to remove from each participating instance all files that were last successfully installed. So CodeDeploy rolls back deployments by redeploying a previously deployed revision of an application **as a new deployment on the failed instances**.

The *CodeDeploy agent* is a software package that, when installed and configured on an instance, makes it possible for that instance to be used in CodeDeploy deployments. The CodeDeploy agent archives revisions and log files on instances. The CodeDeploy agent cleans up these artifacts to conserve disk space.

You can configure CodeDeploy using configuration file `appspec.yml`. For example of EC2, you can define **version**, **os**, **files** (you want to copy in deployment), and **hooks** (scripts you want to run during the deployment). Notice that `appspec.yml` must be placed in the root of the directory of your revision.

For the hooks in AppSpec file, those hook are run in a specfic order know as the **run order**. The following picture is run order of hooks in a **in-place** deployment.<br>
<img src="screenshots/run-order-in-place.png" alt="run-order-in-place"/>
- **BlockTraffic** event means internet traffic is blocked from accessing instances that are currently serving traffic (de-register instances from a load balancer). This event is reserved for the CodeDeploy agent and cannot be used to run scripts.
- **ValidateService** is the last deployment lifecycle event. It is used to verify the deployment was completed successfully.
- **AllowTraffic** event means internet traffic is allowed to access instances after a deployment (register instances with a load balancer). This event is reserved for the CodeDeploy agent and cannot be used to run scripts.

The following picture is run order of hooks in a **blue/green** deployment.<br>
<img src="screenshots/run-order-blue-green.png" alt="run-order-blue-green"/>

### CodeDeploy Lab
1. Services -> IAM -> Roles -> Create role:
    - Select type of trusted entity: AWS service.
    - Choose a use case: EC2.
    - Attach permissions policies: **AmazonS3FullAccess**.
    - Role name: `myS3Role`.
1. Create another role named `cdServiceRole`. Use case is **CodeDeploy** with **AWSCodeDeployRole** policy (default).
1. Launch a new instance with `myS3Role` IAM role and a tag `AppName: mywebapp`. Security group has 2 rule: SSH and HTTP both are open to all.
1. Open your terminal and connet to your EC2 instance.
    ```
    ssh -i kp.pem ec2-user@12.123.123.123
    ```
1. Install CodeDeploy agent on your EC2 instance.
    ```
    sudo yum update
    sudo yum install ruby
    sudo yum install wget
    cd /home/ec2-user
    wget https://aws-codedeploy-us-west-2.s3.amazonaws.com/latest/install
    chmod +x ./install
    sudo ./install auto
    sudo service codedeploy-agent status
    ```
1. Go back to AWS console and create a IAM user named `cdUser` with **Programmatic access** type and **AWSCodeDeployFullAccess** and **AmazonS3FullAccess** policies. Copy the access key and secret key, we will use them in next step.
1. Open a new terminal and configure your aws local setting using access key and secret key you created in previous step. And region is `us-west-2`.
    ```
    aws configure
    ```
1. Go back to AWS console and create a S3 bucket with default setting.
1. We are going to deploy this [application](mywebapp/), we need to create zip and load it into CodeDeploy.
    ```
    cd mywebapp
    aws deploy create-application --application-name mywebapp
    aws deploy push --application-name mywebapp --s3-location s3://[YOUR_BUCKET_NAME]/webapp.zip --ignore-hidden-files
    ```
1. Check your S3 bucket, you should have a zip file named **webapp.zip**. And go to CodeDeploy console, you should also find **mywebapp** in applications.
1. Click **mywebapp** and click **Create deployment group**:
    - Deployment group name: `mydg`.
    - Service role: cdServiceRole.
    - Deployment type: In-place.
    - Environment configuration: check **Amazon EC2 instances** and set up tag group `AppName: mywebapp`.
    - Load balancer: uncheck **Enable load balancing**.
    - Click **Create** deployment group. 
1. Click **Create deployment**. For **Revision location**, choose the S3 bucket you created, it should like `s3://codedeploy-chuck/webapp.zip?eTag=da424dbdc4e2af15e241b725be6ac481`.
1. Go to EC2 console and copy your instance IP4 address and visit it then you should see the results. If you cannot see the result, you can check **Deployment lifecycle events** section in your deployment, click **View events** then you can see what stpe is wrong. The common issue is **Access Denied**, it happened when you forget to give role correct premissions like **AmazonS3FullAccess**.
1. Please don't delete IAM and key-pair resources in this lab, we will use them in next lab.

For monitoring, you can detect and react to changes in pipeline state with CloudWatch Events. CloudWatch Events are composed of *rules* and *targets*. Examples of CloudWatch Events rules and targets:
- A rule that sends a notification when the instance state changes, where an EC2 instance is the event source and SNS is the event target.
- A rule that sends a notification when the build phase changes, where a CodeBuild configuration is the event source and SNS is the event target.
- A rule that detects pipeline changes and invokes an Lambda function.

### CodePipeline Lab
1. Create a bucket with **US West (Oregon)** region.
1. Upload [cf-template.json](cloudpipeline/cf-template.json) to your bucket.
1. Services -> IAM -> Policies -> Create policy, choose JSON and paste the content in [cf-access.json](cloudpipeline/cf-access.json). Name this policy `CFAccess` and create it.
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": "cloudformation:*",
                "Resource": "*"
            },
            {
                "Effect": "Allow",
                "Action": "iam:*",
                "Resource": "*"
            },
            {
                "Action": "ec2:*",
                "Effect": "Allow",
                "Resource": "*"
            }
        ]
    }
    ```
1. Select IAM user `cdUser` and add permission, choose **attach existing policies directly**. Find **CFAccess** and check it then add permission. 
1. Open your terminal and create a stack. Remember to replace `$[YOUR_BUCKET_NAME]` with your bucket name. And notice that `kp` parameter, you should use the key pair name which is used to access your EC2 instance. 
    ```
    aws cloudformation create-stack --stack-name CodeDeployDemoStack \
        --template-url http://s3-us-west-2.amazonaws.com/$[YOUR_BUCKET_NAME]/cf-template.json \
        --parameters ParameterKey=InstanceCount,ParameterValue=1 \
        ParameterKey=InstanceType,ParameterValue=t2.micro \
        ParameterKey=KeyPairName,ParameterValue=kp \
        ParameterKey=OperatingSystem,ParameterValue=Linux \
        ParameterKey=SSHLocation,ParameterValue=0.0.0.0/0 \
        ParameterKey=TagKey,ParameterValue=Name \
        ParameterKey=TagValue,ParameterValue=CodeDeployDemo \
        --capabilities CAPABILITY_IAM
    ```
1. You can use following command to check status.
    ```
    aws cloudformation describe-stacks --stack-name CodeDeployDemoStack --query "Stacks[0].StackStatus" --output text
    ```
1. Create another bucket named `cpbucket` and click **Keep all versions of an object in the same bucket** in versioning.
1. Upload [mywebapp v1 zip](cloudpipeline/mywebapp-version/mywebapp-1.0/mywebapp.zip) to **cpbucket**. You can check the original code [here](cloudpipeline/mywebapp-version/mywebapp-1.0/).
1. Go back your terminal and connect to your EC2 instance created by CloudFormation (its name is **CodeDeployDemo**).
    ```
    ssh -i kp.pem ec2-user@12.123.123.123
    ```
1. Check your CodeDeploy agent has correctly installed.
    ```
    sudo service codedeploy-agent status
    ```
1. Services -> CodeDeploy -> Applications -> Create application:
    - Application name: mywebapp (please delete **mywebapp** you created in previous lab first).
    - Compute platform: EC2/On-premises.
1. Create deployment group:
    - Deployment group name: `mydg`.
    - Service role: cdServiceRole.
    - Deployment type: In-place.
    - Environment configuration: check **Amazon EC2 instances** and set up tag group `Name: CodeDeployDemo`.
    - Load balancer: uncheck **Enable load balancing**.
    - Click **Create** deployment group.
1. Click **Create deployment**. For **Revision location**, type the S3 bucket you created, it should like `s3://cpbucket/mywebapp.zip`.
1. After deployment finished, you can visit EC2 instance IP4 address and then check the result.
1. Now we are going to upload the 2nd version of **mywebapp**, please upload [mywebapp v2 zip](cloudpipeline/mywebapp-version/mywebapp-2.0/mywebapp.zip) to your S3 bucket. You can switch **Versions** to **Show** to see the versioning of **mywebapp**.
1. Services -> CodeDeploy -> Pipeline -> Getting started -> Create pipeline:
    - Pipeline name: `mypipeline`.
    - Source provider: S3.
    - Bucket: **cpbucket**.
    - S3 object key: `mywebapp.zip`.
    - In **Add build stage** step, click **Skip build stage**.
    - Deploy provider: AWS CodeDeploy.
    - Region: US West (Oregon).
    - Application name: `mywebapp`.
    - Deployment group: `mydg`.
1. After pipeline finished, you can visit EC2 instance address again and then check the result, you should see the version 2 page.
1. Now let's upload the 3rd version, please upload [mywebapp v3 zip](cloudpipeline/mywebapp-version/mywebapp-3.0/mywebapp.zip) to your S3 bucket.
1. After uploading, you can go back to CodePipeline console to see the process is triggered automatically.
1. When deployment is complete, you can visit EC2 instance address again and see the version 3 page.

### ECS
Elastic Container Service (ECS) is AWS container service which allows you run your docker images in the cloud. You can run your containers on EC2 or Fargate (serverless). Elastic Container Registry (ECR) is where you can store your container images like [Docker Hub](https://hub.docker.com).

*Port mappings* allow containers to access ports on the host container instance to send or receive traffic. Port mappings are specified as part of the container definition. ALB uses dynamic port mapping, so you can run multiple tasks from a single service on the same container instance.

If you want to access other AWS services, the execution role must be attached to the ECS task.

If you want to run a serverless data store service on 2 docker containers using shared memory, you have to put the 2 containers into a single task definition using a Fargate launch type. For question about when should you put multiple containers into the same task definition versus deploying containers separately in multiple task definitions, you should put multiple containers in the same task definition if:

- Containers share a common lifecycle (that is, they should be launched and terminated together).
- Containers are required to be run on the same underlying host (that is, one container references the other on a localhost port).
- You want your containers to share resources.
- Your containers share data volumes.

Otherwise, you should define your containers in separate tasks definitions so that you can scale, provision, and deprovision them separately.

### CodeBuild Lab
1. Services -> CodeCommit -> Repositories -> Create repository with named `mysourcecode`.
1. Create an IAM user named `ccUser` with **Programmatic access** type and **AWSCodeCommitFullAccess** and **AmazonEC2ContainerRegistryPowerUser** policies. Copy the access key and secret key, we will use them in next step.
1. Open a new terminal and configure your aws local setting using access key and secret key you created in previous step. And region is `us-west-2`.
    ```
    aws configure
    ```
1. Then please follow the official document [here](https://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html#setting-up-ssh-unixes-keys). You can save key in file like `~/.ssh/codecommit_rsa`.
1. Clone the repo you created.
    ```
    cd codebuild
    git clone ssh://git-codecommit.us-west-2.amazonaws.com/v1/repos/mysourcecode
    ```
1. Copy [Dockerfile](codebuild/Dockerfile) and [buildspec.yml](codebuild/buildspec.yml) to the local repo.
    ```
    cp Dockerfile ./mysourcecode
    cp buildspec.yml ./mysourcecode
    ```
1. Commit and push.
    ```
    cd mysourcecode
    git add .
    git commit -m "Adding Dockerfile and buildspec.yml"
    git push
    ```
1. Services -> ECS -> Clusters -> Create Cluster:
    - Select **EC2 Linux + Networking**.
    - Cluster name: `mycluster`.
    - Provisioning Model: On-Demand Instance.
    - EC2 instance type: **t2.micro**.
1. Click your cluster **mycluster** and switch to tab **ECS Instances**, you can check container and EC2 instance.
1. Click **Repositories** (ECR) in left panel and create repository with name `mydockerrepo`.
1. Choose (instead of clicking) **mydockerrepo** repo and click **View push commands**, then you see the steps. Follow those steps to push commands for **mydockerrepo**.
1. If finished, you can click your image repo and see your **latest** tag. Copy image URL of latest tag like `820658239912.dkr.ecr.us-west-2.amazonaws.com/mydockerrepo:latest`, you will use it in next step.
1. Now go back to **Cluster** -> **Task Definitions** -> Create new Task Definition:
    - Select EC2.
    - Task Definition Name: `mytaskdefinition`.
    - Task memory (MiB): 512.
    - Task CPU (unit): 512.
    - Click **Add container**:
        - Container name: `mycontainer`.
        - Image: paste image URL you copied in prevous step.
        - Port mappings: both **Host port** and **Container port** are set to `80`.
        - Click **Add**.
    - Click **Create**.
1. Select your task **mytaskdefinition:1** -> Actions -> Create Service:
    - Launch type: EC2.
    - Cluster: `mycluster`.
    - Service name: `myservice`.
    - Number of tasks: 1.
1. Clusters -> click **mycluster** -> switch to **ECS Instances** tab -> click the URL of EC2 Instance, you will be redirected to EC2 instance.
1. And visit your EC2 instance public IP address to see the result.
1. You can use [buildspec.yml](codebuild/buildspec.yml) to tell AWS how to build your image instead of doing manually. Now go to your repo by `cd codebuild/mysourcecode`. You can find `buildspec.yml` under this folder, please update the docker tag like `820658239912.dkr.ecr.us-west-2.amazonaws.com/mydockerrepo:latest`, there are 2 places you need to update.
1. Commit and push this change.
    ```
    cd codebuild/mysourcecode
    git add buildspec.yml
    git commit -m "Updating buildspec.yml"
    git push
    ```
1. Services -> CodeBuild -> BUild project -> Create build project:
    - Project name: `mycodebuild`.
    - Source provider: AWS CodeCommit.
    - Repository: `mysourcecode`.
    - Environment image: Managed image.
    - Operating system: Ubuntu.
    - Runtime(s): Standard.
    - Image: aws/codebuild/standard:4.0.
    - Privileged: check **Enable this flag if you want to build Docker images or want your builds to get elevated privileges**.
    - Buildpsec: choose **Use a buildspec file**. Notice this is important feature, if you don't have permission to access the source code or you don't want to use `buildspec` in this repo, you can choose **Insert build commit** to write your script to trigger this code base.
    - Then click **Create build project**.
1. Select your build project and click **Start build**. Choose **master** for branch and leave everything as default then click **Start build**.
1. Now you might see your build failed, that's because the role created by CodeBuild doesn't have correct permission to access. So go to IAM role and select **codebuild-mycodebuild-service-role**. Please attch a policy **AmazonEC2ContainerRegistryPowerUser** on it.
1. After attaching the policy, go back to your failed build and click **Retry build**.
1. You can visit the EC2 instance IP4 address again to see the result. And you can check your **latest** tag is changed to image you just created in ECR.

### CloudFormation
For basic concept, please refer to [here](../../level-1/aws-certified-cloud-practitioner/README.md#cloudformation). The main [sections](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-anatomy.html) in CloudFormation template (you can refer this example [template](cloudformation/template.yml)):
- `Parameters`: Parameters enable you to input custom values to your template each time you create or update a stack. You can reference this value by using `!Ref` or `Ref:`.
- `Mappings`: Set mapping values based on a region.
- `Conditions`: Set up condition to determine action. e.g. provision resources based on environment.
- `Transform`: For serverless applications, it specifies the version of the AWS SAM to use. When you specify a transform, you can use SAM syntax to declare resources in your template. The model defines the syntax that you can use and how it is processed. You can also reference code located in S3 to allow for code reuse. e.g. for lambda code or template [snippets](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/CHAP_TemplateQuickRef.html) of CloudFormation code.
- `Resources`: The AWS resources to create.
- `Outputs`: Describes the values that are returned whenever you view your stack's properties.

CloudFormation **nested stacks** are stacks created as part of other stacks. They allow you to reuse your CloudFormation code so you don't need to copy/paste every time. You can create a nested stack within another stack (template) stored in S3 by using the `AWS::CloudFormation::Stack` resource type, please see example [here](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/resource-import-nested-stacks.html).

By default, the *automatic rollback on error* feature is enabled. Sometimes you need to prevent CloudFormation from deleting your entire stack on failure. You can disable rollback on failure only when you create a new stack. You can't disable rollback on failure for an existing stack.
- Use `--disable-rollback` flag with the AWS CLI.
- For console, on the **Configure stack options** page, in the **Advanced options** section, choose **Stack creation options**. For **Rollback on failure**, choose **Disabled**.

With the *DeletionPolicy* attribute you can preserve or (in some cases) backup a resource when its stack is deleted. You can prevent a stack from being accidentally deleted by enabling *termination protection* on the entire stack, not preservation of a specific resource.

How is CloudFormation different from Elastic Beanstalk? 
- Elastic Beanstalk provides an environment to easily deploy and run applications in the cloud. It is integrated with developer tools and provides a one-stop experience for you to manage the lifecycle of your applications.
- CloudFormation is a convenient provisioning mechanism for a broad range of AWS and third party resources. It supports the infrastructure needs of many different types of applications such as existing enterprise applications, legacy applications, applications built using a variety of AWS resources and container-based solutions (including those built using Elastic Beanstalk).

### CloudFormation Lab
1. Services -> CloudFormation -> Stacks -> create stack with new resources:
    - Prepare template: choose **Template is ready**.
    - Template source: choose **Upload a template file** and upload [template.yml](cloudformation/template.yml). Notice that before uploading, you need to replace `ImageId` value with an AMI avaible in your region. You can get `ImageId` value from EC2 launch instance page.
    - Stack name: `myStack`.
    - KeyName: choose your existing key pair (e.g. **kp**).
1. When status is changed to **CREATE_COMPLETE**, you can go to EC2 console to check whether instance is created.
1. Open your terminal, then you can access the EC2 instance.
    ```
    ssh -i kp.pem ec2-user@12.123.123.123
    ```

### Serverless Application Model (SAM)
Serverless Application Model (SAM) is an extension to CloudFormation used to define serverless applications. You can use the SAM CLI to package and deploy.
- `sam package`: It packages your application and upload to S3.
- `sam deploy`: It deploys your serverless app using CloudFormation.

SAM supports the following resource types:
- `AWS::Serverless::Api`: It creates a collection of API Gateway resources and methods that can be invoked through HTTPS endpoints.
- `AWS::Serverless::Application`: It embeds a serverless application from the [Serverless Application Repository](https://serverlessrepo.aws.amazon.com/applications) or from an Amazon S3 bucket as a nested application.
- `AWS::Serverless::Function`: It creates a Lambda function, IAM execution role, and event source mappings that trigger the function.
- `AWS::Serverless::HttpApi`: It creates an API Gateway HTTP API, which enables you to create RESTful APIs with lower latency and lower costs than REST APIs.
- `AWS::Serverless::LayerVersion`: It creates a Lambda LayerVersion that contains library or runtime code needed by a Lambda Function.
- `AWS::Serverless::SimpleTable`: It creates a DynamoDB table with a single attribute primary key. It is useful when data only needs to be accessed via a primary key.
- `AWS::Serverless::StateMachine`: It creates an Step Functions state machine, which you can use to orchestrate Lambda functions and other AWS resources to form complex and robust workflows.

UserPool applies to the Cognito service which is used for authentication for mobile app and web. There is no resource named UserPool in the Serverless Application Model.

An AWS SAM template file closely follows the format of an AWS CloudFormation template file. AWS SAM templates can include several major sections. Only the Transform and Resources sections are required.
- Transform (required): The declaration Transform: `AWS::Serverless-2016-10-31` is required for AWS SAM template files. This declaration identifies an AWS CloudFormation template file as an AWS SAM template file.
- Resources (required): This section can contain a combination of AWS CloudFormation resources and AWS SAM resources.
- Globals: This section is unique to AWS SAM. It defines properties that are common to all your serverless functions and APIs. All the `AWS::Serverless::Function`, `AWS::Serverless::Api`, and `AWS::Serverless::SimpleTable` resources inherit the properties that are defined in the Globals section.
- Parameters: Objects that are declared in the `Parameters` section cause the `sam deploy --guided` command to present additional prompts to the user.
- Description: This section is same with CloudFormtaion.
- Metadata: This section is same with CloudFormtaion.
- Mappings: This section is same with CloudFormtaion.
- Conditions: This section is same with CloudFormtaion.
- Outputs: This section is same with CloudFormtaion.

### SAM Lab
1. Install SAM CLI, please follow this [document](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).
1. Create a IAM user with **Programmatic access** access type and **IAMFullAccess**, **AWSLambdaFullAccess**, and **AWSCloudFormationFullAccess** policies.
1. Set up access key and secret key to your loacl configuration.
    ```
    aws configure
    ```
1. Create a bucket.
    ```
    aws s3 mb s3://cfsambucket --region us-west-2
    ```
1. Package the [lambda.yml](sam/lambda.yml) to template named `sam-template.yml`.
    ```
    cd sam
    sam package \
        --template-file lambda.yml \
        --output-template-file sam-template.yml \
        --s3-bucket cfsambucket
    ```
1. Deploy the package `sam-template.yml`.
    ```
    sam deploy \
        --template-file sam-template.yml \
        --stack-name myStack \
        --capabilities CAPABILITY_IAM
    ```
1. Services -> Lambda -> click function you created -> in **Function code**, click **Test** and give it a name for creating test event. Then click **Test** again you can see the result.

## CloudWatch
CloudWatch is a monitoring service to monitor your AWS resources and applications. The host level metrics consists of CPU, network, disk and status check. RAM utilization is a *custom metric*. You can retrieve data from any terminated EC2 or ELB instance after its termination. CloudWatch can be used on-premises not restricted to just AWS resources.

For metric granularity, you have 1 minute for detailed monitoring and 5 minutes for standard monitoring. The minimum resolution supported by CloudWatch is 1-second data points, which is a *high-resolution* metric.

For retention period of all metrics:
- Data points with a period of less than 60 seconds are available for 3 hours. Only custom metrics that you publish to CloudWatch are available at high resolution.
- Data points with a period of 60 seconds (1 minute) are available for 15 days.
- Data points with a period of 300 seconds (5 minute) are available for 63 days.
- Data points with a period of 3600 seconds (1 hour) are available for 455 days (15 months). 

CloudWatch Logs lets you monitor and troubleshoot your systems and applications using your existing system, application and custom log files. The CloudWatch Logs Agent will send log data every 5 seconds by default and is configurable by the user. CloudWatch Logs by default are stored indefinitely. You can use *Metric Filter* to define the terms and patterns to look for in log data as it is sent to CloudWatch Logs.

Notice following AWS services are different:
- AWS CloudWatch: It monitors performance.
- AWS CloudTrail: It monitors API calls in the AWS platform.
- AWS Config: It records the state of your AWS environment and can notify you of changes.

### CloudWatch Lab
1. Create a IAM role named `CloudWatchEC2` with **CloudWatchFullAccess** policy and used by **EC2**.
1. Launch a new instance using role **CloudWatchEC2** you created, and paste following script for **User data**. For rules of security group, choose **SSH** and **HTTP** with open all.
    ```sh
    #!/bin/bash
    yum update -y
    sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
    cd /home/ec2-user/
    curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
    unzip CloudWatchMonitoringScripts-1.2.2.zip
    rm -rf CloudWatchMonitoringScripts-1.2.2.zip
    ```
1. Connect to your EC2 instance.
    ```
    chmod 400 MyKeyPair.pem
    ssh -i MyKeyPair.pem ec2-user@12.123.123.123
    ```
1. You can verify the metric.
    ```
    /home/ec2-user/aws-scripts-mon/mon-put-instance-data.pl --mem-util --verify --verbose
    ```
1. Let's send metrics, you need to wait 5 minutes to see the result on CloudWatch by default.
    ```
    /home/ec2-user/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail
    ```
1. Services -> CloudWatch -> Metrics -> Custom Namespaces -> click **System/Linux** -> click **InstnaceId** then you can see the 3 metrics you sent.
1. If you want to create data every minute, please edit `crontab` first.
    ```
    sudo su
    cd /etc
    nano crontab
    ```
1. Paste following script at the bottom and hit `Ctrl+X` to exit.
    ```
    */1 * * * * root /home/ec2-user/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail
    ```
1. Services -> CloudWatch -> Metrics -> Custom Namespaces -> click **System/Linux** -> click **InstnaceId** then you can see the metrics you sent.

## Exam Information
- [AWS Certified Developer–Associate
(DVA-C01) Examination Guide](https://d1.awsstatic.com/training-and-certification/docs-dev-associate/AWS_Certified_Developer_Associate-Exam_Guide_EN_1.4.pdf).
    - 130 minutes in length.
    - 65 scenario based questions.
    - Results immediately.
    - Passmark is 720, top score 1000.
    - $150 USD.
    - Multiple choice.
    - Qualification is valid for 2 years.
- [AWS FAQs](https://aws.amazon.com/faqs/).
- [AWS Whitepapers](https://aws.amazon.com/whitepapers/).
- [AWS re:Invent](https://www.youtube.com/results?search_query=aws+reinvent).
- [AWS Recertification](https://aws.amazon.com/certification/recertification/).

## References
- A Cloud Guru course [AWS Certified Developer - Associate 2020](https://acloud.guru/learn/aws-certified-developer-associate)
- Udemy course [Practice Exams | AWS Certified Developer Associate 2020](https://www.udemy.com/course/aws-certified-developer-associate-practice-tests-dva-c01/)
- AWS official documentation
